# Description

This project is part of Programming User Interfaces course at School of Electrical Engineering, University of Belgrade. The goal is to go through all the stages of UI design (analysis, prototype and implementation). The accent is on UI design therefore attention is focused on designing interface and no database is created, all data is static and hardcoded in program. The requirements are to design desktop application as well as mobile application.

## Phase I : User requrements analysis

Documentation on functionalities required by the user can be found [here](https://docs.google.com/document/d/1FJ_Fa7UV0EDzlxsCs5-2hvPsNVV5rGEDbiRpO4TZRnk/edit?usp=sharing).

## Phase II : Creating Prototype

Prototype for both mobile and desktop app can be found inside prototype directory in the root of this repository. Prototypes are classified into two directories depending on the platform. Besides Pencil project, there is also an html version of prototype inside this directories.

