package rs.ac.bg.etf.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.RestaurantData;
import rs.ac.bg.etf.models.Meal;

public class SearchMeals extends Page {

    private static final int FIELD_WIDTH = 180;

    private static final int LABEL_WIDTH = 60;

    private BorderPane layout;

    private Map<String, String> searchParams;

    public SearchMeals(Stage window, int width, int height) {
        super(window, width, height);
        window.setTitle(Main.APP_TITLE + " - Search Meals");
        searchParams = new HashMap<>();
        searchParams.put("category", "All");
        searchParams.put("ingredients", "");
        searchParams.put("cuisine", "All");
    }

    @Override
    public Scene getScene() {
        layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout(RestaurantData.getInstance().getMeals()));

        return new Scene(layout, width, height);
    }

    private BorderPane createCentralLayout(List<Meal> meals) {
        BorderPane pane = new BorderPane();
        pane.setTop(createSearchPane(searchParams));
        ScrollPane center = new ScrollPane(Utils.createMenuPane(meals));
        center.setHbarPolicy(ScrollBarPolicy.NEVER);
        center.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        pane.setCenter(center);

        return pane;
    }

    private GridPane createSearchPane(Map<String, String> searchParams) {
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setHgap(5);
        grid.setVgap(5);

        Label label = new Label("Category:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 0);

        ComboBox<String> categories = new ComboBox<>();
        categories.setMinWidth(FIELD_WIDTH);
        categories.getItems().addAll("All", "Breakfast", "Lunch", "Dinner");
        categories.getSelectionModel().select(searchParams.get("category"));
        categories.setOnAction(e -> searchParams.put("category", categories.getSelectionModel().getSelectedItem()));
        grid.add(categories, 1, 0);

        label = new Label("Cuisine:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 2, 0);

        ComboBox<String> cuisines = new ComboBox<>();
        cuisines.setMinWidth(FIELD_WIDTH);
        cuisines.getItems().addAll(RestaurantData.getInstance().getAllCuisines());
        cuisines.getSelectionModel().select(searchParams.get("cuisine"));
        cuisines.setOnAction(e -> searchParams.put("cuisine", cuisines.getSelectionModel().getSelectedItem()));
        grid.add(cuisines, 3, 0);

        label = new Label("Ingredients:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        TextField name = new TextField(searchParams.get("ingredients"));
        name.setMaxWidth(FIELD_WIDTH);
        name.textProperty().addListener((observable, oldValue, newValue) -> searchParams.put("ingredients", newValue));
        grid.add(name, 1, 1);

        Button search = new Button("Search");
        search.setOnAction(e -> search());
        grid.add(search, 4, 1);

        return grid;
    }

    private void search() {
        layout.setCenter(createCentralLayout(filterMeals(searchParams)));
    }

    private List<Meal> filterMeals(Map<String, String> searchParameters) {
        List<Meal> results = new ArrayList<>(RestaurantData.getInstance().getMeals());

        for (Meal m : RestaurantData.getInstance().getMeals()) {
            if (!searchParameters.get("category").toLowerCase().equals("all")
                    && !searchParameters.get("category").toLowerCase().equals(m.getCategory())) {
                results.remove(m);
            }
            if (!searchParameters.get("cuisine").toLowerCase().equals("all")
                    && !listContainsKeyword(searchParameters.get("cuisine"), m.getCuisine())) {
                results.remove(m);
            }
            if (!searchParameters.get("ingredients").equals("")
                    && !listContainsList(Arrays.asList(searchParameters.get("ingredients").toLowerCase().split(" ")),
                            m.getIngredients())) {
                results.remove(m);
            }
        }

        return results;
    }

    private boolean listContainsKeyword(String keyword, List<String> list) {
        for (String word : list) {
            if (word.toLowerCase().equals(keyword.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    private boolean listContainsList(List<String> keywords, List<String> list) {
        for (String kw : keywords) {
            if (!listContainsKeyword(kw, list)) {
                return false;
            }
        }

        return true;
    }
}
