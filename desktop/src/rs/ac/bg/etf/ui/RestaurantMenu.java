package rs.ac.bg.etf.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;

public class RestaurantMenu extends RestaurantPreview {

    public RestaurantMenu(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Restaurant Menu");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private Pane createCentralLayout() {
        BorderPane layout = new BorderPane();
        ScrollPane pane = new ScrollPane(Utils.createMenuPane(restaurant.getMeals()));
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        layout.setCenter(pane);
        layout.setBottom(createBottomLayout());

        return layout;
    }

    private HBox createBottomLayout() {
        HBox bottom = new HBox();
        bottom.setPadding(new Insets(10, 0, 10, 0));
        bottom.setAlignment(Pos.BOTTOM_CENTER);
        Button ok = new Button("OK");
        ok.setAlignment(Pos.BOTTOM_CENTER);
        ok.setOnAction(e -> Main.restaurantWindow.close());
        bottom.getChildren().add(ok);

        return bottom;
    }
}
