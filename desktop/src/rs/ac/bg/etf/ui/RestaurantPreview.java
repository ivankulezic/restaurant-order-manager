package rs.ac.bg.etf.ui;

import java.util.LinkedHashMap;
import java.util.Map;

import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rs.ac.bg.etf.models.Restaurant;

public abstract class RestaurantPreview {

    public static Restaurant restaurant;

    protected int width;

    protected int height;

    private static Map<String, String> menuButtons;

    static {
        menuButtons = new LinkedHashMap<>();
        menuButtons.put("Overview", "browse");
        menuButtons.put("Menu", "menu");
        menuButtons.put("Comments", "comments");
        menuButtons.put("Photos", "picture");
    }

    protected Stage window;

    public RestaurantPreview(Stage window, int width, int height) {
        this.window = window;
        this.width = width;
        this.height = height;
    }

    public abstract Scene getScene();

    protected VBox createSideBar() {
        VBox menu = new VBox();
        menu.setPrefSize(170, 50);
        menu.setStyle("-fx-background-color:" + Utils.BACKGROUND_COLOR);

        for (Map.Entry<String, String> button : menuButtons.entrySet()) {
            menu.getChildren().add(Utils.createMenuItem(button.getValue(), button.getKey()));
        }

        return menu;
    }
}
