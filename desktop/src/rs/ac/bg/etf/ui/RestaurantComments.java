package rs.ac.bg.etf.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;

public class RestaurantComments extends RestaurantPreview {

    public RestaurantComments(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Restaurant Comments");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private Pane createCentralLayout() {

        ScrollPane pane = new ScrollPane(Utils.createCommentsPane(RestaurantPreview.restaurant.getComments()));
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        BorderPane layout = new BorderPane();
        layout.setCenter(pane);
        layout.setBottom(createBottomLayout());

        return layout;
    }

    private Pane createBottomLayout() {
        VBox pane = new VBox();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.setAlignment(Pos.CENTER);
        Button ok = new Button("OK");
        ok.setOnAction(e -> Main.restaurantWindow.close());
        ok.setAlignment(Pos.BOTTOM_CENTER);
        pane.getChildren().add(ok);

        return pane;
    }
}
