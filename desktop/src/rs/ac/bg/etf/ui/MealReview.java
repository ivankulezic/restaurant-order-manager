package rs.ac.bg.etf.ui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.ReviewData;
import rs.ac.bg.etf.models.Meal;
import rs.ac.bg.etf.models.RestaurantComment;
import rs.ac.bg.etf.models.Review;

public class MealReview {
    private int width;

    private int height;

    private Stage window;

    private Meal meal;

    private Review review;

    private static final int FIELD_WIDTH = 250;

    private static final String ERROR_STYLE = "-fx-border-color: red ; -fx-border-width: 0.5px ;";

    public MealReview(Stage window, int width, int height, Meal meal, Review review) {
        this.window = window;
        this.width = width;
        this.height = height;
        this.meal = meal;
        this.review = review;
    }

    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Rate Meal");
        BorderPane layout = new BorderPane();
        layout.setCenter(createCenterLayout());

        return new Scene(layout, width, height);
    }

    private Pane createCenterLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Label label = new Label(meal.getName());
        label.setFont(new Font(24));
        GridPane.setHalignment(label, HPos.CENTER);
        label.setAlignment(Pos.TOP_CENTER);
        grid.add(label, 1, 0);

        label = new Label("Rating:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        ComboBox<Integer> rating = new ComboBox<>();
        rating.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        rating.getSelectionModel().select(new Integer(10));
        rating.setMinWidth(80);
        grid.add(rating, 1, 1);

        label = new Label("Title:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 2);

        TextField title = new TextField();
        title.setMinWidth(FIELD_WIDTH + 50);
        grid.add(title, 1, 2);

        label = new Label("Comment:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 3);

        TextField comment = new TextField();
        comment.setMinWidth(FIELD_WIDTH + 50);
        comment.setMinHeight(150);
        grid.add(comment, 1, 3);

        Text errorMessage = new Text();
        grid.add(errorMessage, 1, 4);

        HBox buttonPane = new HBox(170);

        Button cancel = new Button("Cancel");
        cancel.setOnAction(e -> Main.mealWindow.close());

        Button confirm = new Button("Confirm");
        confirm.setOnAction(e -> {
            errorMessage.setText("");
            errorMessage.setFill(Color.FIREBRICK);
            title.setStyle("");
            comment.setStyle("");

            if (title.getText().equals("")) {
                title.setStyle(ERROR_STYLE);
                errorMessage.setText("Please fill all fields.");
                return;
            }
            if (comment.getText().equals("")) {
                comment.setStyle(ERROR_STYLE);
                errorMessage.setText("Please fill all fields.");
                return;
            }
            meal.addComment(new RestaurantComment(title.getText(), comment.getText(),
                    "" + rating.getSelectionModel().getSelectedItem(), review.getUsername()));
            ReviewData.getInstance().removeReview(review);
            Main.mealWindow.close();
            Main.switchScene(Main.window, new RateMeals(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
        });
        buttonPane.getChildren().addAll(cancel, confirm);
        grid.add(buttonPane, 1, 5);

        return grid;
    }

}
