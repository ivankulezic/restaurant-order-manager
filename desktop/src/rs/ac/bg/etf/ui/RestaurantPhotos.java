package rs.ac.bg.etf.ui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;

public class RestaurantPhotos extends RestaurantPreview {

    ImageView imageView;

    public RestaurantPhotos(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Restaurant Photos");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());

        BorderPane inner = new BorderPane();
        inner.setCenter(createCentralLayout());
        inner.setBottom(createBottomLayout());

        layout.setCenter(inner);
        return new Scene(layout, width, height);
    }

    private ScrollPane createCentralLayout() {
        GridPane layout = new GridPane();
        layout.setPadding(new Insets(25, 25, 25, 25));
        layout.setVgap(20);
        layout.setHgap(10);

        String restaurant = RestaurantPreview.restaurant.getName().toLowerCase().replace(" ", "_");

        imageView = new ImageView();
        imageView.setImage(new Image(Utils.IMAGES_LOCATION + restaurant + "1_big.jpg"));
        GridPane.setHalignment(imageView, HPos.CENTER);

        layout.add(imageView, 0, 0, 5, 1);

        for (int i = 0; i < 4; i++) {
            final int imageNo = i + 1;
            ImageView view = new ImageView();
            view.setOnMouseClicked(e -> changeImage(restaurant, imageNo));
            view.setImage(new Image(Utils.IMAGES_LOCATION + restaurant + imageNo + "_small.jpg"));

            layout.add(view, i, 1, 1, 1);
        }

        ScrollPane pane = new ScrollPane(layout);
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        return pane;
    }

    private Pane createBottomLayout() {
        VBox pane = new VBox();
        pane.setAlignment(Pos.BOTTOM_CENTER);
        pane.setPadding(new Insets(10, 10, 10, 10));
        Button ok = new Button("OK");
        GridPane.setHalignment(ok, HPos.CENTER);
        ok.setOnAction(e -> Main.restaurantWindow.close());
        pane.getChildren().add(ok);

        return pane;
    }

    private void changeImage(String restaurant, int imageNo) {
        imageView.setImage(new Image(Utils.IMAGES_LOCATION + restaurant + imageNo + "_big.jpg"));
    }

}
