package rs.ac.bg.etf.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.RestaurantData;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.User;

public class Register extends Page {

    private static final int FIELD_WIDTH = 250;

    private static final String ERROR_STYLE = "-fx-border-color: red ; -fx-border-width: 0.5px ;";

    private static String[] fields = { "First Name", "Last Name", "Mobile Phone", "Street", "City", "Username",
            "Password" };

    private ComboBox<String> city;

    public Register(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Register");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private GridPane createCentralLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        List<TextField> inputFields = new ArrayList<>();

        for (int i = 0; i < fields.length; i++) {
            Label label = new Label(fields[i] + ":");
            label.setMinWidth(100);
            label.setAlignment(Pos.CENTER_RIGHT);
            grid.add(label, 0, i);

            if (!fields[i].equals("City")) {
                TextField textInput = !fields[i].equals("Password") ? new TextField() : new PasswordField();
                textInput.promptTextProperty().set(fields[i]);
                textInput.setMinWidth(FIELD_WIDTH);
                grid.add(textInput, 1, i);
                inputFields.add(textInput);
            } else {
                city = new ComboBox<>();
                city.getItems().addAll(RestaurantData.getInstance().getAllCities());
                city.getSelectionModel().select(0);
                city.setPrefWidth(FIELD_WIDTH);
                grid.add(city, 1, i);
            }

        }

        Text errorMessage = new Text();
        grid.add(errorMessage, 1, fields.length, 2, 1);

        Button register = new Button("Create Account");
        register.setAlignment(Pos.CENTER);
        register.setMinWidth(FIELD_WIDTH);
        register.setOnAction(e -> register(inputFields, errorMessage));
        grid.add(register, 1, fields.length + 1);

        return grid;
    }

    private void register(List<TextField> fields, Text message) {
        message.setText("");
        message.setFill(Color.FIREBRICK);

        boolean noEmptyFields = true;

        for (TextField field : fields) {
            field.setStyle("");
            if (field.getText().isEmpty()) {
                message.setText("Please fill all fields before submitting.");
                noEmptyFields = false;
                field.setStyle(ERROR_STYLE);
            }
        }

        if (!noEmptyFields) {
            return;
        }

        if (!UserData.getInstance().checkUsername(fields.get(4).getText())) {
            message.setText("Username is already taken.");
            return;
        }

        Map<String, String> address = new HashMap<>();
        address.put("city", city.getSelectionModel().getSelectedItem());
        address.put("street", fields.get(3).getText());

        User user = new User(fields.get(4).getText(), fields.get(5).getText(), fields.get(0).getText(),
                fields.get(1).getText(), fields.get(2).getText(), address);
        UserData userData = UserData.getInstance();

        Utils.showPopUp("Registration Successful", "Redirecting You to the main page...", AlertType.INFORMATION);

        userData.addUser(user);
        userData.setLoggedIn(true);
        userData.setUsername(fields.get(4).getText());

        Main.switchScene(Main.window, new BrowseRestaurants(window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
    }
}
