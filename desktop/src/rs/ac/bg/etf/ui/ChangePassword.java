package rs.ac.bg.etf.ui;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.User;

public class ChangePassword extends Page {

    private static final int FIELD_WIDTH = 250;

    private static final int LABEL_WIDTH = 150;

    public ChangePassword(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Change Password");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private GridPane createCentralLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Map<String, String> passwordInfo = new HashMap<>();

        Label label = new Label("Old Password:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 0);

        TextField oldPassword = new PasswordField();
        oldPassword.setMinWidth(FIELD_WIDTH);
        oldPassword.textProperty().addListener((observable, oldValue, newValue) -> passwordInfo.put("old", newValue));
        grid.add(oldPassword, 1, 0);

        label = new Label("New Password:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        TextField newPassword = new PasswordField();
        newPassword.setPrefWidth(FIELD_WIDTH);
        newPassword.textProperty().addListener((observable, oldValue, newValue) -> passwordInfo.put("new", newValue));
        grid.add(newPassword, 1, 1);

        label = new Label("Confirm New Password:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 2);

        TextField confirmNew = new PasswordField();
        confirmNew.textProperty()
                .addListener((observable, oldValue, newValue) -> passwordInfo.put("confirm", newValue));
        grid.add(confirmNew, 1, 2);

        Text errorMessage = new Text();
        grid.add(errorMessage, 1, 3);

        Button register = new Button("Change Password");
        register.setAlignment(Pos.CENTER);
        register.setMinWidth(FIELD_WIDTH);
        register.setOnAction(e -> changePassword(passwordInfo, errorMessage));
        grid.add(register, 1, 4);

        return grid;
    }

    private void changePassword(Map<String, String> profileInformation, Text errorMessage) {
        errorMessage.setText("");
        errorMessage.setFill(Color.FIREBRICK);

        if (profileInformation.size() < 3) {
            errorMessage.setText("Please fill all the fields.");
            return;
        }

        User u = UserData.getInstance().getUser();

        if (!profileInformation.get("old").equals(u.getPassword())) {
            errorMessage.setText("Incorrect old password.");
            return;
        }

        if (!profileInformation.get("new").equals(profileInformation.get("confirm"))) {
            errorMessage.setText("New password must match confirm password.");
            return;
        }

        u.setPassword(profileInformation.get("new"));
        UserData.getInstance().saveData();

        Utils.showPopUp("Password Change Successful", "Your password was updated successfully!", AlertType.INFORMATION);
    }
}
