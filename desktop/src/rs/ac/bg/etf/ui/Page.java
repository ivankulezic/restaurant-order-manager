package rs.ac.bg.etf.ui;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rs.ac.bg.etf.controllers.UserData;

public abstract class Page {

    protected int width;

    protected int height;

    private static Map<String, String> menuButtonsLoggedIn;

    private static Map<String, String> menuButtonsNotLoggedIn;

    static {
        menuButtonsLoggedIn = new LinkedHashMap<>();
        menuButtonsLoggedIn.put("Welcome $user", "user");
        menuButtonsLoggedIn.put("View Profile", "profile");
        menuButtonsLoggedIn.put("Change Password", "password");
        menuButtonsLoggedIn.put("Logout", "logout");
        menuButtonsLoggedIn.put("Browse Restaurants", "browse");
        menuButtonsLoggedIn.put("Search Restaurants", "search");
        menuButtonsLoggedIn.put("Search Meals", "search");
        menuButtonsLoggedIn.put("View Cart", "cart");
        menuButtonsLoggedIn.put("View Orders", "orders");
        menuButtonsLoggedIn.put("Review Restaurants", "review");
        menuButtonsLoggedIn.put("Rate Meals", "review");

        menuButtonsNotLoggedIn = new LinkedHashMap<>();
        menuButtonsNotLoggedIn.put("Browse Restaurants", "browse");
        menuButtonsNotLoggedIn.put("Search Restaurants", "search");
        menuButtonsNotLoggedIn.put("Search Meals", "search");
        menuButtonsNotLoggedIn.put("Login", "login");
        menuButtonsNotLoggedIn.put("Register", "register");
    }

    protected Stage window;

    public Page(Stage window, int width, int height) {
        this.window = window;
        this.width = width;
        this.height = height;
    }

    public abstract Scene getScene();

    protected VBox createSideBar() {
        VBox menu = new VBox();
        menu.setPrefSize(170, 50);
        menu.setStyle("-fx-background-color:" + Utils.BACKGROUND_COLOR);

        Map<String, String> buttons = UserData.getInstance().isLoggedIn() ? menuButtonsLoggedIn
                : menuButtonsNotLoggedIn;

        Map<String, String> template = new HashMap<>();
        template.put("\\$user", UserData.getInstance().getUsername());

        for (Map.Entry<String, String> button : buttons.entrySet()) {
            menu.getChildren()
                    .add(Utils.createMenuItem(button.getValue(), Utils.renderTemplate(button.getKey(), template)));
        }

        return menu;
    }
}