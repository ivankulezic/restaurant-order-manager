package rs.ac.bg.etf.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.Cart;

public class CartOverview extends Page {

    public CartOverview(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Cart");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private BorderPane createCentralLayout() {
        BorderPane layout = new BorderPane();
        layout.setBottom(createBottomLayout());

        ScrollPane pane = new ScrollPane(Utils.createCartPane(Cart.getInstance().getMeals()));
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        layout.setCenter(pane);

        return layout;
    }

    private HBox createBottomLayout() {
        HBox bottom = new HBox(20);
        bottom.setPadding(new Insets(10, 10, 10, 350));
        bottom.setAlignment(Pos.CENTER_RIGHT);
        Button checkout = new Button("Checkout");
        checkout.setOnAction(e -> {
            if (!Cart.getInstance().getMeals().isEmpty()) {
                Main.switchScene(Main.window, new Checkout(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            } else {
                new Alert(AlertType.INFORMATION, "Please add some items to the cart before checking out.",
                        ButtonType.OK).showAndWait();
            }
        });
        Label l = new Label("TOTAL: " + Cart.getInstance().getTotal() + ",00 RSD");
        l.setAlignment(Pos.CENTER_RIGHT);
        bottom.getChildren().addAll(l, checkout);

        return bottom;
    }

}
