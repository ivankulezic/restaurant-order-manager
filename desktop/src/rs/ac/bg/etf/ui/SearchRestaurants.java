package rs.ac.bg.etf.ui;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.RestaurantData;
import rs.ac.bg.etf.models.Restaurant;

public class SearchRestaurants extends Page {

    private static final int FIELD_WIDTH = 120;

    private static final int LABEL_WIDTH = 60;

    private BorderPane layout;

    private Map<String, String> searchParams;

    public SearchRestaurants(Stage window, int width, int height) {
        super(window, width, height);
        window.setTitle(Main.APP_TITLE + " - Search Restaurants");
        searchParams = new HashMap<>();
        searchParams.put("searchTerm", "");
        searchParams.put("selectedCity", "All");
        searchParams.put("selectedArea", "All");
        searchParams.put("selectedCuisine", "All");
        searchParams.put("selectedRating", "Any");
        searchParams.put("cardSelected", "false");
        searchParams.put("cashSelected", "false");
    }

    @Override
    public Scene getScene() {
        layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout(RestaurantData.getInstance().getRestaurants()));

        return new Scene(layout, width, height);
    }

    private BorderPane createCentralLayout(List<Restaurant> restaurants) {
        BorderPane pane = new BorderPane();
        pane.setTop(createSearchPane(searchParams));
        ScrollPane center = new ScrollPane(Utils.createRestaurantPane(restaurants));
        center.setHbarPolicy(ScrollBarPolicy.NEVER);
        center.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        pane.setCenter(center);

        return pane;
    }

    private GridPane createSearchPane(Map<String, String> searchParams) {
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setHgap(5);
        grid.setVgap(5);

        Label label = new Label("Name:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 0);
        TextField name = new TextField(searchParams.get("searchTerm"));
        name.setMaxWidth(FIELD_WIDTH);
        name.textProperty().addListener((observable, oldValue, newValue) -> searchParams.put("searchTerm", newValue));
        grid.add(name, 1, 0);

        RestaurantData restaurantData = RestaurantData.getInstance();

        label = new Label("City:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 2, 0);

        ComboBox<String> city = new ComboBox<>();
        city.setMinWidth(FIELD_WIDTH);
        List<String> cities = restaurantData.getAllCities();
        Collections.sort(cities);
        cities.add(0, "All");
        city.getItems().addAll(cities);
        city.getSelectionModel().select(searchParams.get("selectedCity"));
        city.setOnAction(e -> searchParams.put("selectedCity", city.getSelectionModel().getSelectedItem()));
        grid.add(city, 3, 0);

        label = new Label("Area:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 4, 0);

        ComboBox<String> area = new ComboBox<>();
        area.setMinWidth(FIELD_WIDTH);
        List<String> areas = restaurantData.getAllAreas();
        Collections.sort(areas);
        areas.add(0, "All");
        area.getItems().addAll(areas);
        area.getSelectionModel().select(searchParams.get("selectedArea"));
        area.setOnAction(e -> searchParams.put("selectedArea", area.getSelectionModel().getSelectedItem()));
        grid.add(area, 5, 0, 2, 1);

        label = new Label("Cuisine:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        ComboBox<String> cuisine = new ComboBox<>();
        cuisine.setMinWidth(FIELD_WIDTH);
        List<String> cuisines = restaurantData.getAllCuisines();
        Collections.sort(cuisines);
        cuisines.add(0, "All");
        cuisine.getItems().addAll(cuisines);
        cuisine.getSelectionModel().select(searchParams.get("selectedCuisine"));
        cuisine.setOnAction(e -> searchParams.put("selectedCuisine", cuisine.getSelectionModel().getSelectedItem()));
        grid.add(cuisine, 1, 1);

        label = new Label("Rating:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 2, 1);
        ComboBox<String> rating = new ComboBox<>();
        rating.setMinWidth(FIELD_WIDTH);
        List<String> ratings = IntStream.range(1, 11).mapToObj(i -> "" + i).collect(Collectors.toList());
        ratings.add(0, "Any");
        rating.getItems().addAll(ratings);
        rating.getSelectionModel().select(searchParams.get("selectedRating"));
        rating.setOnAction(e -> searchParams.put("selectedRating", rating.getSelectionModel().getSelectedItem()));
        grid.add(rating, 3, 1);

        label = new Label("Payment:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 4, 1);

        CheckBox cash = new CheckBox("cash");
        cash.setSelected(Boolean.valueOf(searchParams.get("cashSelected")));
        cash.setOnAction(e -> searchParams.put("cashSelected", String.valueOf(cash.isSelected())));
        grid.add(cash, 5, 1);
        CheckBox card = new CheckBox("card");
        card.setSelected(Boolean.valueOf(searchParams.get("cardSelected")));
        card.setOnAction(e -> searchParams.put("cardSelected", String.valueOf(card.isSelected())));
        grid.add(card, 6, 1);

        Button search = new Button("Search");
        search.setOnAction(e -> search());
        grid.add(search, 6, 2);

        return grid;
    }

    private void search() {
        layout.setCenter(createCentralLayout(RestaurantData.getInstance().filter(searchParams)));
    }
}
