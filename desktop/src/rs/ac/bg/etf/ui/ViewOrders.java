package rs.ac.bg.etf.ui;

import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.OrderData;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.Order;

public class ViewOrders extends Page {

    private static final int LABEL_WIDTH = 100;

    public ViewOrders(Stage window, int width, int height) {
        super(window, width, height);
        window.setTitle(Main.APP_TITLE + " - View Orders");
    }

    @Override
    public Scene getScene() {
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private ScrollPane createCentralLayout() {
        ScrollPane layout = new ScrollPane(
                createOrderPane(OrderData.getInstance().getOrders(UserData.getInstance().getUsername())));
        layout.setHbarPolicy(ScrollBarPolicy.NEVER);
        layout.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        return layout;
    }

    private VBox createOrderPane(List<Order> orders) {
        VBox panes = new VBox();
        panes.setPadding(new Insets(10, 10, 10, 80));

        HBox pane = new HBox(20);
        pane.setPadding(new Insets(10, 10, 10, 10));

        Label label = new Label("DATE");
        label.setPrefWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER);
        pane.getChildren().add(label);

        label = new Label("TOTAL");
        label.setPrefWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER);
        pane.getChildren().add(label);

        label = new Label("STATUS");
        label.setPrefWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER);
        pane.getChildren().add(label);

        label = new Label("DETAILS");
        label.setPrefWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER);
        pane.getChildren().add(label);

        panes.getChildren().add(pane);

        for (Order o : orders) {
            pane = new HBox(20);
            pane.setPadding(new Insets(10, 10, 10, 10));

            label = new Label(o.getDate());
            label.setPrefWidth(LABEL_WIDTH);
            label.setAlignment(Pos.CENTER);
            pane.getChildren().add(label);

            label = new Label(o.getTotal());
            label.setPrefWidth(LABEL_WIDTH);
            label.setAlignment(Pos.CENTER);
            pane.getChildren().add(label);

            label = new Label(o.getStatus());
            label.setPrefWidth(LABEL_WIDTH);
            label.setAlignment(Pos.CENTER);
            pane.getChildren().add(label);

            HBox buttonWrapper = new HBox();
            buttonWrapper.setPadding(new Insets(0, 10, 0, 10));
            Button details = new Button("Details");
            details.setOnAction(
                    e -> Main.switchScene(Main.mealWindow, new OrderDetails(Main.mealWindow, 500, 600, o).getScene()));
            buttonWrapper.getChildren().add(details);
            buttonWrapper.setPrefWidth(LABEL_WIDTH);
            buttonWrapper.setAlignment(Pos.CENTER);
            pane.getChildren().add(buttonWrapper);

            panes.getChildren().add(pane);
        }

        return panes;
    }
}
