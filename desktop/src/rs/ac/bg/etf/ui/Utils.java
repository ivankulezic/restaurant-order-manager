package rs.ac.bg.etf.ui;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.Cart;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.Meal;
import rs.ac.bg.etf.models.Restaurant;
import rs.ac.bg.etf.models.RestaurantComment;

public class Utils {

    public static final String ICONS_LOCATION = "/rs/ac/bg/etf/resources/icons/";

    public static final String IMAGES_LOCATION = "/rs/ac/bg/etf/resources/images/";

    public static final String BACKGROUND_COLOR = "#caccce";

    public static String renderTemplate(String text, Map<String, String> template) {
        for (Map.Entry<String, String> entry : template.entrySet()) {
            text = text.replaceAll(entry.getKey(), entry.getValue());
        }

        return text;
    }

    public static HBox createMenuItem(String icon, String buttonText) {

        Button button = new Button(buttonText);
        button.setGraphic(new ImageView(new Image(ICONS_LOCATION + icon + ".png")));
        button.setContentDisplay(ContentDisplay.LEFT);
        button.setPrefSize(170, 50);
        button.setOnAction(e -> navigate(buttonText));

        return new HBox(button);
    }

    public static void navigate(String pageName) {
        switch (pageName) {
        case "Register":
            Main.switchScene(Main.window, new Register(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Login":
            Main.switchScene(Main.window, new Login(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Logout":
            UserData.getInstance().logout();
            Main.switchScene(Main.window,
                    new BrowseRestaurants(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Browse Restaurants":
            Main.switchScene(Main.window,
                    new BrowseRestaurants(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Search Restaurants":
            Main.switchScene(Main.window,
                    new SearchRestaurants(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "View Profile":
            Main.switchScene(Main.window, new ProfileOverview(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Change Password":
            Main.switchScene(Main.window, new ChangePassword(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Search Meals":
            Main.switchScene(Main.window, new SearchMeals(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Menu":
            Main.switchScene(Main.restaurantWindow,
                    new RestaurantMenu(Main.restaurantWindow, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Overview":
            Main.switchScene(Main.restaurantWindow,
                    new RestaurantOverview(Main.restaurantWindow, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Photos":
            Main.switchScene(Main.restaurantWindow,
                    new RestaurantPhotos(Main.restaurantWindow, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Comments":
            Main.switchScene(Main.restaurantWindow,
                    new RestaurantComments(Main.restaurantWindow, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "View Cart":
            Main.switchScene(Main.window, new CartOverview(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "View Orders":
            Main.switchScene(Main.window, new ViewOrders(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Review Restaurants":
            Main.switchScene(Main.window, new RateRestaurants(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        case "Rate Meals":
            Main.switchScene(Main.window, new RateMeals(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            break;
        }
    }

    public static void showPopUp(String title, String message, AlertType type) {
        Alert popUp = new Alert(type, message, ButtonType.OK);
        popUp.setTitle(title);
        popUp.showAndWait();
    }

    public static VBox createRestaurantPane(List<Restaurant> restaurants) {
        VBox panes = new VBox();

        for (Restaurant r : restaurants) {
            GridPane pane = new GridPane();
            Insets padding = new Insets(10, 10, 10, 10);
            pane.setPadding(padding);

            VBox left = new VBox(new ImageView(new Image(IMAGES_LOCATION + r.getImage())));
            left.setAlignment(Pos.CENTER);
            left.setPadding(padding);
            pane.add(left, 0, 0);

            VBox right = new VBox();
            right.setAlignment(Pos.CENTER_LEFT);
            Label title = new Label(r.getName().toUpperCase());
            title.setFont(new Font(24));
            Label description = new Label(r.getDescription());
            description.setPadding(padding);
            right.getChildren().addAll(title, description);
            pane.add(right, 1, 0, 2, 1);

            pane.add(new Label("RATING: " + r.getRating() + " / 10"), 1, 1);

            Button details = new Button("Details");
            details.setOnAction(e -> {
                RestaurantPreview.restaurant = r;
                Main.openSceneInNewStage(Main.restaurantWindow,
                        new RestaurantOverview(Main.restaurantWindow, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            });
            GridPane.setHalignment(details, HPos.RIGHT);
            pane.add(details, 2, 1);

            panes.getChildren().add(pane);
        }

        return panes;
    }

    public static VBox createMenuPane(List<Meal> meals) {
        VBox panes = new VBox();

        for (Meal m : meals) {
            GridPane pane = new GridPane();
            Insets padding = new Insets(10, 10, 10, 10);
            pane.setPadding(padding);

            VBox left = new VBox(new ImageView(new Image(IMAGES_LOCATION + m.getImage())));
            left.setAlignment(Pos.CENTER);
            left.setPadding(padding);
            pane.add(left, 0, 0);

            VBox right = new VBox(10);
            right.setAlignment(Pos.CENTER_LEFT);
            Label title = new Label(m.getName());
            title.setFont(new Font(20));
            Label description = new Label(m.getDescription());
            description.setPadding(padding);

            HBox buttons = new HBox(150);
            Button add = new Button("Add to Cart");
            add.setOnAction(e -> {
                if (UserData.getInstance().isLoggedIn()) {
                    Cart.getInstance().add(m);
                } else {
                    Alert alert = new Alert(AlertType.INFORMATION,
                            "You must be logged in to make orders. Do you want to go to login page?", ButtonType.YES,
                            ButtonType.NO);
                    Optional<ButtonType> answer = alert.showAndWait();
                    if (answer.get().equals(ButtonType.YES)) {
                        Main.restaurantWindow.close();
                        Main.switchScene(Main.window,
                                new Login(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
                    }

                }
            });

            Button details = new Button("Details");
            details.setOnAction(e -> Main.openSceneInNewStage(Main.mealWindow,
                    new MealOverview(Main.mealWindow, 500, 600, m).getScene()));
            buttons.getChildren().addAll(add, details);

            right.getChildren().addAll(title, description, new Label("PRICE: " + m.getPrice() + " RSD"), buttons);
            pane.add(right, 1, 0, 2, 1);

            panes.getChildren().add(pane);
        }

        return panes;
    }

    public static VBox createCommentsPane(List<RestaurantComment> comments) {
        VBox panes = new VBox();
        panes.setAlignment(Pos.CENTER);
        panes.setPadding(new Insets(20, 20, 20, 20));

        for (RestaurantComment c : comments) {
            VBox pane = new VBox(10);
            Insets padding = new Insets(10, 10, 10, 10);
            pane.setPadding(padding);

            Label label = new Label(c.getTitle());
            label.setFont(new Font(24));
            pane.getChildren().add(label);

            label = new Label("Rating: " + c.getRating() + " / 10");
            label.setPadding(new Insets(0, 0, 0, 50));
            pane.getChildren().add(label);

            label = new Label(c.getDescription());
            label.setPadding(new Insets(0, 0, 0, 160));
            pane.getChildren().add(label);

            label = new Label("User: " + c.getUser());
            label.setPadding(new Insets(0, 0, 0, 50));
            pane.getChildren().add(label);

            panes.getChildren().add(pane);
        }

        return panes;
    }

    public static VBox createCartPane(Map<Meal, Integer> meals) {
        VBox panes = new VBox();

        for (Map.Entry<Meal, Integer> entry : meals.entrySet()) {
            GridPane pane = new GridPane();
            VBox left = new VBox(new ImageView(new Image(Utils.IMAGES_LOCATION + entry.getKey().getImage())));
            left.setPadding(new Insets(10, 10, 10, 10));
            pane.add(left, 0, 0);

            Label title = new Label(entry.getKey().getName());
            title.setPadding(new Insets(50, 0, 50, 0));
            title.setFont(new Font(20));
            title.setAlignment(Pos.CENTER_LEFT);
            title.setPrefWidth(200);
            pane.add(title, 1, 0);

            VBox wrapper = new VBox();
            wrapper.setPadding(new Insets(60, 0, 50, 0));
            TextField count = new TextField("" + entry.getValue());
            count.setEditable(false);
            count.setAlignment(Pos.CENTER_LEFT);
            count.setMaxWidth(40);
            wrapper.getChildren().add(count);
            pane.add(wrapper, 2, 0);

            Label price = new Label(" X " + entry.getKey().getPrice() + ",00 RSD");
            price.setPadding(new Insets(60, 0, 50, 0));
            price.setAlignment(Pos.CENTER_LEFT);
            pane.add(price, 3, 0);

            wrapper = new VBox();
            wrapper.setPadding(new Insets(60, 0, 50, 10));
            Button remove = new Button("Remove");
            remove.setAlignment(Pos.CENTER_LEFT);
            remove.setPrefWidth(70);
            remove.setOnAction(e -> removeFromCart(entry.getKey()));
            wrapper.getChildren().add(remove);
            pane.add(wrapper, 4, 0);

            panes.getChildren().add(pane);
        }

        return panes;
    }

    private static void removeFromCart(Meal m) {
        Cart.getInstance().remove(m);
        Main.switchScene(Main.window, new CartOverview(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
    }
}
