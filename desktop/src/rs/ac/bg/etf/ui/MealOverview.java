package rs.ac.bg.etf.ui;

import java.util.List;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.Cart;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.Meal;
import rs.ac.bg.etf.models.RestaurantComment;

public class MealOverview {

    private int width;

    private int height;

    private Stage window;

    private Meal meal;

    public MealOverview(Stage window, int width, int height, Meal m) {
        this.window = window;
        this.width = width;
        this.height = height;
        this.meal = m;
    }

    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Meal Overview");
        BorderPane layout = new BorderPane();

        layout.setBottom(createBottomLayout());

        ScrollPane pane = new ScrollPane(createCentralLayout());
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);

        layout.setCenter(pane);

        return new Scene(layout, width, height);
    }

    private Pane createBottomLayout() {
        HBox pane = new HBox();
        pane.setPadding(new Insets(10, 10, 10, 10));
        Button ok = new Button("OK");
        pane.setAlignment(Pos.BOTTOM_CENTER);
        pane.getChildren().add(ok);
        ok.setOnAction(e -> Main.mealWindow.close());

        return pane;
    }

    private Pane createCentralLayout() {
        VBox pane = new VBox(10);
        pane.setAlignment(Pos.CENTER);

        HBox top = new HBox(10);
        VBox imagePane = new VBox(10);
        imagePane.setPadding(new Insets(30, 10, 10, 10));
        imagePane.setAlignment(Pos.CENTER);
        imagePane.getChildren().addAll(new ImageView(new Image(Utils.IMAGES_LOCATION + meal.getImage())),
                new Label("RATING: " + meal.getRating() + " / 10"));
        top.getChildren().add(imagePane);

        VBox textPane = new VBox(10);
        textPane.setAlignment(Pos.TOP_LEFT);
        textPane.setPadding(new Insets(30, 10, 10, 10));
        Label name = new Label(meal.getName());
        name.setFont(new Font(24));
        Label description = new Label(meal.getDescription());
        description.setPadding(new Insets(0, 0, 0, 10));
        textPane.getChildren().addAll(name, description);
        top.getChildren().add(textPane);

        pane.getChildren().add(top);

        GridPane center = new GridPane();
        center.setVgap(10);
        center.setHgap(20);
        center.setPadding(new Insets(0, 0, 0, 20));

        Label label = new Label("Category:");
        GridPane.setHalignment(label, HPos.RIGHT);
        center.add(label, 0, 0);

        label = new Label(meal.getCategory());
        GridPane.setHalignment(label, HPos.LEFT);
        center.add(label, 1, 0);

        label = new Label("Cuisine:");
        GridPane.setHalignment(label, HPos.RIGHT);
        center.add(label, 0, 1);

        label = new Label(convertToString(meal.getCuisine()));
        GridPane.setHalignment(label, HPos.LEFT);
        center.add(label, 1, 1);

        label = new Label("Ingredients:");
        GridPane.setHalignment(label, HPos.RIGHT);
        center.add(label, 0, 2);

        label = new Label(convertToString(meal.getIngredients()));
        GridPane.setHalignment(label, HPos.LEFT);
        center.add(label, 1, 2);

        ComboBox<Integer> count = new ComboBox<>();
        count.getItems().addAll(2, 3, 4, 5);
        count.getSelectionModel().select(0);
        center.add(count, 0, 3);

        Button add = new Button("Add to Cart");
        add.setOnAction(e -> addToCart(meal, count.getSelectionModel().getSelectedItem()));
        center.add(add, 1, 3);

        label = new Label("Comments:");
        GridPane.setHalignment(label, HPos.LEFT);
        center.add(label, 0, 4);

        pane.getChildren().add(center);

        for (RestaurantComment c : meal.getComments()) {
            HBox p = new HBox(30);
            p.setPadding(new Insets(10, 10, 10, 50));
            p.setAlignment(Pos.CENTER_LEFT);
            label = new Label(c.getUser());
            label.setPrefWidth(100);
            label.setAlignment(Pos.CENTER_RIGHT);
            p.getChildren().add(label);
            label = new Label(c.getDescription());
            label.setAlignment(Pos.CENTER_LEFT);
            label.setPrefWidth(250);
            p.getChildren().add(label);

            pane.getChildren().add(p);
        }

        return pane;
    }

    private void addToCart(Meal m, int n) {
        if (UserData.getInstance().isLoggedIn()) {
            for (int i = 0; i < n; i++) {
                Cart.getInstance().add(m);
            }
        } else {
            Alert alert = new Alert(AlertType.INFORMATION,
                    "You must be logged in to make orders. Do you want to go to login page?", ButtonType.YES,
                    ButtonType.NO);
            Optional<ButtonType> answer = alert.showAndWait();
            if (answer.get().equals(ButtonType.YES)) {
                Main.mealWindow.close();
                Main.switchScene(Main.window, new Login(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
            }
        }
    }

    private String convertToString(List<String> list) {
        String result = "";

        for (String s : list) {
            result += s + ", ";
        }

        return result.substring(0, result.length() - 2);
    }
}
