package rs.ac.bg.etf.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.RestaurantData;
import rs.ac.bg.etf.controllers.ReviewData;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.Restaurant;
import rs.ac.bg.etf.models.Review;

public class RateRestaurants extends Page {

    public RateRestaurants(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Rate Restaurants");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private ScrollPane createCentralLayout() {
        ScrollPane pane = new ScrollPane(createReviewPane());
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);

        return pane;
    }

    public static VBox createReviewPane() {
        VBox panes = new VBox();

        for (Review r : ReviewData.getInstance().getRestaurantReviews(UserData.getInstance().getUsername())) {
            Restaurant restaurant = RestaurantData.getInstance().getRestaurant(r.getRestaurant());
            GridPane pane = new GridPane();
            VBox left = new VBox(new ImageView(new Image(Utils.IMAGES_LOCATION + restaurant.getImage())));
            left.setPadding(new Insets(10, 10, 10, 10));
            pane.add(left, 0, 0);

            Label title = new Label(restaurant.getName());
            title.setPadding(new Insets(50, 0, 50, 0));
            title.setFont(new Font(20));
            title.setAlignment(Pos.CENTER_LEFT);
            title.setPrefWidth(400);
            pane.add(title, 1, 0);

            VBox wrapper = new VBox();
            wrapper.setPadding(new Insets(60, 0, 50, 10));
            Button rate = new Button("Rate");
            rate.setOnAction(e -> Main.switchScene(Main.mealWindow,
                    new RestaurantReview(Main.mealWindow, 500, 450, restaurant, r).getScene()));
            rate.setAlignment(Pos.CENTER_LEFT);
            wrapper.getChildren().add(rate);
            pane.add(wrapper, 2, 0);

            panes.getChildren().add(pane);
        }

        return panes;
    }
}
