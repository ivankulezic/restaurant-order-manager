package rs.ac.bg.etf.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.UserData;

public class Login extends Page {

    private static final String ERROR_STYLE = "-fx-border-color: red ; -fx-border-width: 0.5px ;";

    public Login(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Login");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private GridPane createCentralLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Label userName = new Label("Username:");
        grid.add(userName, 0, 1);

        TextField username = new TextField();
        username.setMinWidth(250);
        grid.add(username, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField password = new PasswordField();
        grid.add(password, 1, 2);

        Button login = new Button("Log in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(login);
        grid.add(hbBtn, 1, 4);

        Text errorMessage = new Text();
        grid.add(errorMessage, 1, 6);

        login.setOnAction(e -> login(username, password, errorMessage));

        return grid;
    }

    private void login(TextField username, PasswordField password, Text message) {
        message.setText("");
        message.setFill(Color.FIREBRICK);
        username.setStyle("");
        password.setStyle("");
        ;

        if (username.getText().isEmpty()) {
            username.setStyle(ERROR_STYLE);
        }

        if (password.getText().isEmpty()) {
            password.setStyle(ERROR_STYLE);
        }

        if (username.getText().isEmpty() || password.getText().isEmpty()) {
            message.setText("Please fill all fields before submitting.");
            return;
        }

        if (!UserData.getInstance().checkCredentials(username.getText().trim(), password.getText())) {
            message.setText("Incorrect credentials.");
            return;
        }

        UserData.getInstance().setLoggedIn(true);
        UserData.getInstance().setUsername(username.getText().trim());

        Main.switchScene(Main.window, new BrowseRestaurants(window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
    }

}
