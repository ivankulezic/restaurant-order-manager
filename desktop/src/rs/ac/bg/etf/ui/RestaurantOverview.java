package rs.ac.bg.etf.ui;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.service.geocoding.GeocoderStatus;
import com.lynden.gmapsfx.service.geocoding.GeocodingResult;
import com.lynden.gmapsfx.service.geocoding.GeocodingService;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;

public class RestaurantOverview extends RestaurantPreview implements MapComponentInitializedListener {

    private GoogleMapView mapView;

    public RestaurantOverview(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Restaurant Overview");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private BorderPane createCentralLayout() {
        BorderPane pane = new BorderPane();

        GridPane top = new GridPane();
        Insets padding = new Insets(10, 10, 10, 10);
        top.setPadding(padding);

        VBox left = new VBox(new ImageView(new Image(Utils.IMAGES_LOCATION + restaurant.getImage())),
                new Label("RATING: " + restaurant.getRating() + " / 10"));
        left.setSpacing(10);
        left.setAlignment(Pos.CENTER);
        left.setPadding(padding);
        top.add(left, 0, 0);

        VBox right = new VBox();
        right.setAlignment(Pos.TOP_LEFT);
        right.setPadding(new Insets(20, 0, 0, 0));
        Label title = new Label(restaurant.getName().toUpperCase());
        title.setFont(new Font(24));
        Label description = new Label(restaurant.getDescription());
        description.setPadding(padding);
        right.getChildren().addAll(title, description);
        top.add(right, 1, 0, 2, 1);

        pane.setTop(top);

        HBox center = new HBox(20);
        center.setAlignment(Pos.CENTER);

        VBox details = new VBox(20);
        details.getChildren().addAll(new Label("Address:"),
                new Label(restaurant.getAddress().get("street") + "\n" + restaurant.getAddress().get("city")),
                new Label("Phone:"), new Label(restaurant.getPhone()), new Label("Email:"),
                new Label(restaurant.getEmail()), new Label("Working Hours"),
                new Label("workdays: " + restaurant.getHours().get("workday") + "\n" + "weekends: "
                        + restaurant.getHours().get("weekend")));

        VBox map = new VBox();
        map.setPrefSize(400, 350);
        mapView = new GoogleMapView();
        mapView.addMapInializedListener(this);
        map.getChildren().add(mapView);

        center.getChildren().addAll(details, map);
        pane.setCenter(center);

        HBox bottom = new HBox();
        bottom.setPadding(new Insets(10, 0, 10, 0));
        bottom.setAlignment(Pos.CENTER);
        Button ok = new Button("OK");
        ok.setOnAction(e -> Main.restaurantWindow.close());
        bottom.getChildren().add(ok);
        pane.setBottom(bottom);

        return pane;
    }

    @Override
    public void mapInitialized() {
        MapOptions mapOptions = new MapOptions();

        GeocodingService geocodingService = new GeocodingService();

        geocodingService.geocode(restaurant.getAddress().get("street"),
                (GeocodingResult[] results, GeocoderStatus status) -> {
                    LatLong coordinates = new LatLong(results[0].getGeometry().getLocation().getLatitude(),
                            results[0].getGeometry().getLocation().getLongitude());

                    mapOptions.center(coordinates).overviewMapControl(false).panControl(false).rotateControl(false)
                            .scaleControl(false).streetViewControl(false).zoomControl(false).zoom(18);

                    GoogleMap map = mapView.createMap(mapOptions);

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(coordinates).visible(Boolean.TRUE).title(restaurant.getName());
                    Marker marker = new Marker(markerOptions);
                    map.addMarker(marker);
                    map.setCenter(coordinates);
                });
    }

}
