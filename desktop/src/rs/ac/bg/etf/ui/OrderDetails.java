package rs.ac.bg.etf.ui;

import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.models.Order;
import rs.ac.bg.etf.models.OrderEntry;

public class OrderDetails {
    private int width;

    private int height;

    private Stage window;

    private Order order;

    private static final int FIELD_WIDTH = 250;

    public OrderDetails(Stage window, int width, int height, Order order) {
        this.window = window;
        this.width = width;
        this.height = height;
        this.order = order;
    }

    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Order Overview");
        BorderPane layout = new BorderPane();
        layout.setTop(createTopLayout());
        layout.setBottom(createBottomLayout());

        ScrollPane pane = new ScrollPane(createOrdersPane(order.getEntries()));
        pane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        pane.setHbarPolicy(ScrollBarPolicy.NEVER);

        layout.setCenter(pane);

        return new Scene(layout, width, height);
    }

    private Pane createBottomLayout() {
        HBox pane = new HBox();
        pane.setPadding(new Insets(10, 10, 10, 10));
        Button ok = new Button("OK");
        pane.setAlignment(Pos.BOTTOM_CENTER);
        pane.getChildren().add(ok);
        ok.setOnAction(e -> Main.mealWindow.close());

        return pane;
    }

    private Pane createTopLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Label label = new Label("Name:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 0);

        TextField name = new TextField(order.getName());
        name.setMinWidth(FIELD_WIDTH);
        name.setEditable(false);
        grid.add(name, 1, 0);

        label = new Label("Mobile Phone:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        TextField mobile = new TextField(order.getPhone());
        mobile.setEditable(false);
        mobile.setMinWidth(FIELD_WIDTH);
        grid.add(mobile, 1, 1);

        label = new Label("Address:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 2);

        TextField street = new TextField(order.getStreet());
        street.setEditable(false);
        street.setMinWidth(FIELD_WIDTH);
        grid.add(street, 1, 2);

        TextField city = new TextField(order.getCity());
        city.setEditable(false);
        city.setPrefWidth(FIELD_WIDTH);
        grid.add(city, 1, 3);

        label = new Label("Payment:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 4);

        TextField payment = new TextField(order.getPayment());
        payment.setEditable(false);
        payment.setPrefWidth(FIELD_WIDTH);
        grid.add(payment, 1, 4);

        label = new Label("Note:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 5);

        TextField note = new TextField(order.getNote());
        note.setEditable(false);
        note.setMinWidth(FIELD_WIDTH);
        grid.add(note, 1, 5);

        label = new Label("TOTAL:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 6);

        label = new Label(order.getTotal());
        label.setFont(new Font(18));
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 1, 6);

        return grid;
    }

    public static VBox createOrdersPane(List<OrderEntry> orderEntries) {
        VBox panes = new VBox();

        for (OrderEntry entry : orderEntries) {
            GridPane pane = new GridPane();
            VBox left = new VBox(new ImageView(new Image(Utils.IMAGES_LOCATION + entry.getMeal().getImage())));
            left.setPadding(new Insets(10, 10, 10, 10));
            pane.add(left, 0, 0);

            Label title = new Label(entry.getMeal().getName());
            title.setPadding(new Insets(50, 0, 50, 0));
            title.setFont(new Font(14));
            title.setAlignment(Pos.CENTER_LEFT);
            title.setPrefWidth(150);
            pane.add(title, 1, 0);

            VBox wrapper = new VBox();
            wrapper.setPadding(new Insets(60, 0, 50, 0));
            TextField count = new TextField("" + entry.getCount());
            count.setEditable(false);
            count.setAlignment(Pos.CENTER_LEFT);
            count.setMaxWidth(40);
            wrapper.getChildren().add(count);
            pane.add(wrapper, 2, 0);

            Label price = new Label(" X " + entry.getMeal().getPrice() + ",00 RSD");
            price.setPadding(new Insets(60, 0, 50, 0));
            price.setAlignment(Pos.CENTER_LEFT);
            pane.add(price, 3, 0);

            panes.getChildren().add(pane);
        }

        return panes;
    }
}
