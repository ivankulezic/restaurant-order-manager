package rs.ac.bg.etf.ui;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.RestaurantData;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.User;

public class ProfileOverview extends Page {

    private static final int FIELD_WIDTH = 250;

    private static final int LABEL_WIDTH = 100;

    public ProfileOverview(Stage window, int width, int height) {
        super(window, width, height);
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Profile Overview");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private GridPane createCentralLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Map<String, String> profileInfo = new HashMap<>();
        profileInfo.put("username", UserData.getInstance().getUsername());

        User user = UserData.getInstance().getUser();

        Label label = new Label("First Name:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 0);

        TextField firstname = new TextField();
        firstname.setMinWidth(FIELD_WIDTH);
        firstname.setText(user.getName());
        firstname.textProperty().addListener((observable, oldValue, newValue) -> profileInfo.put("name", newValue));
        profileInfo.put("name", user.getName());
        grid.add(firstname, 1, 0);

        label = new Label("Last Name:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        TextField lastname = new TextField();
        lastname.setPrefWidth(FIELD_WIDTH);
        lastname.setText(user.getSurname());
        lastname.textProperty().addListener((observable, oldValue, newValue) -> profileInfo.put("surname", newValue));
        profileInfo.put("surname", user.getSurname());
        grid.add(lastname, 1, 1);

        label = new Label("Mobile phone:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 2);

        TextField phone = new TextField();
        phone.setText(user.getPhoneNumber());
        phone.setPrefWidth(FIELD_WIDTH);
        phone.textProperty().addListener((observable, oldValue, newValue) -> profileInfo.put("phoneNumber", newValue));
        profileInfo.put("phoneNumber", user.getPhoneNumber());
        grid.add(phone, 1, 2);

        label = new Label("City:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 3);

        ComboBox<String> city = new ComboBox<>();
        city.setPrefWidth(FIELD_WIDTH);
        city.getItems().addAll(RestaurantData.getInstance().getAllCities());
        city.getSelectionModel().select(user.getAddress().get("city"));
        city.setOnAction(e -> profileInfo.put("city", city.getSelectionModel().getSelectedItem()));
        profileInfo.put("city", user.getAddress().get("city"));
        grid.add(city, 1, 3);

        label = new Label("Street:");
        label.setMinWidth(LABEL_WIDTH);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 4);

        TextField street = new TextField();
        street.setPrefWidth(FIELD_WIDTH);
        street.setText(user.getAddress().get("street"));
        street.textProperty().addListener((observable, oldValue, newValue) -> profileInfo.put("street", newValue));
        profileInfo.put("street", user.getAddress().get("street"));
        grid.add(street, 1, 4);

        Text errorMessage = new Text();
        grid.add(errorMessage, 1, 5);

        Button register = new Button("Update Profile");
        register.setAlignment(Pos.CENTER);
        register.setMinWidth(FIELD_WIDTH);
        register.setOnAction(e -> updateProfile(profileInfo, errorMessage));
        grid.add(register, 1, 6);

        return grid;
    }

    private void updateProfile(Map<String, String> profileInformation, Text errorMessage) {
        errorMessage.setText("");
        errorMessage.setFill(Color.FIREBRICK);

        for (Map.Entry<String, String> entry : profileInformation.entrySet()) {
            if (entry.getValue().equals("")) {
                errorMessage.setText("Please fill all the fields.");
                return;
            }
        }

        User u = UserData.getInstance().getUser();

        u.setName(profileInformation.get("name"));
        u.setSurname(profileInformation.get("surname"));
        u.setPhoneNumber(profileInformation.get("phoneNumber"));
        Map<String, String> address = new HashMap<>();
        address.put("street", profileInformation.get("street"));
        address.put("city", profileInformation.get("city"));
        u.setAddress(address);

        UserData.getInstance().saveData();

        Utils.showPopUp("User Data Update Successful", "Your personal data was successfully updated!",
                AlertType.INFORMATION);
    }
}
