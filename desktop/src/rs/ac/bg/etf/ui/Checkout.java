package rs.ac.bg.etf.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import rs.ac.bg.etf.applications.Main;
import rs.ac.bg.etf.controllers.Cart;
import rs.ac.bg.etf.controllers.OrderData;
import rs.ac.bg.etf.controllers.RestaurantData;
import rs.ac.bg.etf.controllers.ReviewData;
import rs.ac.bg.etf.controllers.UserData;
import rs.ac.bg.etf.models.Meal;
import rs.ac.bg.etf.models.Order;
import rs.ac.bg.etf.models.OrderEntry;
import rs.ac.bg.etf.models.Restaurant;
import rs.ac.bg.etf.models.Review;

public class Checkout extends Page {

    private static final int FIELD_WIDTH = 250;

    private static final String ERROR_STYLE = "-fx-border-color: red ; -fx-border-width: 0.5px ;";

    private Map<String, String> params;

    public Checkout(Stage window, int width, int height) {
        super(window, width, height);
        params = new HashMap<>();
        params.put("status", "delivered");
    }

    @Override
    public Scene getScene() {
        window.setTitle(Main.APP_TITLE + " - Checkout");
        BorderPane layout = new BorderPane();
        layout.setLeft(createSideBar());
        layout.setCenter(createCentralLayout());

        return new Scene(layout, width, height);
    }

    private GridPane createCentralLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));

        List<TextField> inputFields = new ArrayList<>();

        Label label = new Label("Name:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 0);

        TextField name = new TextField(
                UserData.getInstance().getUser().getName() + " " + UserData.getInstance().getUser().getSurname());
        params.put("name", name.getText());
        name.textProperty().addListener((observable, oldValue, newValue) -> params.put("name", newValue));
        name.setMinWidth(FIELD_WIDTH);
        grid.add(name, 1, 0);
        inputFields.add(name);

        label = new Label("Mobile Phone:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 1);

        TextField mobile = new TextField(UserData.getInstance().getUser().getPhoneNumber());
        params.put("mobile", mobile.getText());
        mobile.textProperty().addListener((observable, oldValue, newValue) -> params.put("mobile", newValue));
        mobile.setMinWidth(FIELD_WIDTH);
        grid.add(mobile, 1, 1);
        inputFields.add(mobile);

        label = new Label("Address:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 2);

        TextField street = new TextField(UserData.getInstance().getUser().getAddress().get("street"));
        params.put("street", street.getText());
        street.textProperty().addListener((observable, oldValue, newValue) -> params.put("street", newValue));
        street.setMinWidth(FIELD_WIDTH);
        grid.add(street, 1, 2);
        inputFields.add(street);

        ComboBox<String> city = new ComboBox<>();
        city.getItems().addAll(RestaurantData.getInstance().getAllCities());
        city.getSelectionModel().select(UserData.getInstance().getUser().getAddress().get("city"));
        params.put("city", city.getSelectionModel().getSelectedItem());
        city.setOnAction(e -> params.put("city", city.getSelectionModel().getSelectedItem()));
        city.setPrefWidth(FIELD_WIDTH);
        grid.add(city, 1, 3);

        label = new Label("Payment:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 4);

        HBox paymentPane = new HBox(15);
        final ToggleGroup group = new ToggleGroup();

        RadioButton card = new RadioButton("card");
        card.setToggleGroup(group);
        card.setSelected(true);
        params.put("payment", "card");
        card.setOnAction(e -> params.put("payment", "card"));

        RadioButton cash = new RadioButton("cash");
        cash.setToggleGroup(group);
        cash.setOnAction(e -> params.put("payment", "cash"));

        paymentPane.getChildren().addAll(card, cash);
        grid.add(paymentPane, 1, 4);

        label = new Label("Note:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 5);

        TextField note = new TextField();
        params.put("note", note.getText());
        note.textProperty().addListener((observable, oldValue, newValue) -> params.put("note", newValue));
        note.setMinWidth(FIELD_WIDTH);
        grid.add(note, 1, 5);
        inputFields.add(note);

        label = new Label("TOTAL:");
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 0, 6);

        label = new Label("" + Cart.getInstance().getTotal() + ",00 RSD");
        params.put("total", label.getText());
        label.setFont(new Font(18));
        label.setMinWidth(100);
        label.setAlignment(Pos.CENTER_RIGHT);
        grid.add(label, 1, 6);

        Text errorMessage = new Text();
        grid.add(errorMessage, 1, 7);

        HBox pane = new HBox(80);
        Button cancel = new Button("Cancel");
        cancel.setOnAction(e -> Main.switchScene(Main.window,
                new CartOverview(Main.window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene()));
        cancel.setAlignment(Pos.CENTER);
        cancel.setMinWidth(80);

        Button order = new Button("Order");
        order.setAlignment(Pos.CENTER);
        order.setMinWidth(80);
        pane.getChildren().addAll(cancel, order);
        order.setOnAction(e -> checkout(inputFields, errorMessage));
        grid.add(pane, 1, 8);

        return grid;
    }

    private void checkout(List<TextField> fields, Text message) {
        message.setText("");
        message.setFill(Color.FIREBRICK);

        boolean noEmptyFields = true;

        for (TextField field : fields) {
            field.setStyle("");
            if (field.getText().isEmpty() && !field.equals(fields.get(3))) {
                message.setText("Please fill all fields before submitting.");
                noEmptyFields = false;
                field.setStyle(ERROR_STYLE);
            }
        }

        List<OrderEntry> entries = new ArrayList<>();

        for (Map.Entry<Meal, Integer> mealEntry : Cart.getInstance().getMeals().entrySet()) {
            entries.add(new OrderEntry(mealEntry.getKey(), mealEntry.getValue()));
        }

        String date = new SimpleDateFormat("dd.MM.yyyy.").format(new Date());

        Order order = new Order(params.get("status"), entries, params.get("name"), UserData.getInstance().getUsername(),
                params.get("mobile"), params.get("street"), params.get("city"), params.get("note"), params.get("total"),
                params.get("payment"), date);

        OrderData.getInstance().addOrder(order);
        OrderData.getInstance().saveData();

        createReviews(order);

        if (noEmptyFields) {
            Utils.showPopUp("Order Successfull", "Your order has been accepted.", AlertType.INFORMATION);
            Cart.getInstance().clear();
            Utils.navigate("Browse Restaurants");
        }
    }

    private Restaurant getRestaurant(Meal meal) {
        for (Restaurant r : RestaurantData.getInstance().getRestaurants()) {
            for (Meal m : r.getMeals()) {
                if (m.getName().equals(meal.getName())) {
                    return r;
                }
            }
        }
        return null;
    }

    private void createReviews(Order order) {
        Set<Restaurant> restaurants = new HashSet<>();
        for (OrderEntry entry : order.getEntries()) {
            restaurants.add(getRestaurant(entry.getMeal()));
            ReviewData.getInstance()
                    .addReview(new Review(UserData.getInstance().getUsername(), entry.getMeal().getName(), ""));
        }

        for (Restaurant r : restaurants) {
            ReviewData.getInstance().addReview(new Review(UserData.getInstance().getUsername(), "", r.getName()));
        }

        ReviewData.getInstance().saveData();
    }
}
