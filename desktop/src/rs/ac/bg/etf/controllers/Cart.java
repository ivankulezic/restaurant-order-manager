package rs.ac.bg.etf.controllers;

import java.util.HashMap;
import java.util.Map;

import rs.ac.bg.etf.models.Meal;

public class Cart {
    private static Cart instance;

    private Map<Meal, Integer> meals;

    private Cart() {
        meals = new HashMap<>();
    }

    public static Cart getInstance() {

        if (instance == null) {
            instance = new Cart();
        }

        return instance;
    }

    public void add(Meal m) {
        meals.put(m, meals.getOrDefault(m, 0) + 1);
    }

    public Map<Meal, Integer> getMeals() {
        return meals;
    }

    public void clear() {
        meals = new HashMap<>();
    }

    public int getTotal() {
        int sum = 0;

        for (Map.Entry<Meal, Integer> entry : meals.entrySet()) {
            sum += Integer.parseInt(entry.getKey().getPrice()) * entry.getValue();
        }

        return sum;
    }

    public void remove(Meal m) {
        meals.remove(m);
    }
}
