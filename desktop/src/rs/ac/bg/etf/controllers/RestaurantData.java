package rs.ac.bg.etf.controllers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import rs.ac.bg.etf.models.Meal;
import rs.ac.bg.etf.models.Restaurant;

public class RestaurantData {
    private static final String DATA_LOCATION = "src/rs/ac/bg/etf/resources/data/restaurants.json";

    private static RestaurantData instance;

    private List<Restaurant> restaurants;

    private RestaurantData() {

    }

    public static RestaurantData getInstance() {

        if (instance == null) {
            instance = new RestaurantData();
            instance.initializeData();
        }

        return instance;
    }

    private void initializeData() {
        Gson gson = new Gson();
        try (JsonReader reader = new JsonReader(new FileReader(DATA_LOCATION))) {
            restaurants = gson.fromJson(reader, new TypeToken<List<Restaurant>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public List<String> getAllCities() {
        Set<String> cities = new HashSet<>();
        for (Restaurant r : restaurants) {
            cities.add(r.getCity());
        }

        return new ArrayList<>(cities);
    }

    public List<String> getAllAreas() {
        Set<String> areas = new HashSet<>();
        for (Restaurant r : restaurants) {
            areas.add(r.getArea());
        }

        return new ArrayList<>(areas);
    }

    public List<String> getAllCuisines() {
        Set<String> cuisines = new HashSet<>();
        for (Restaurant r : restaurants) {
            cuisines.addAll(r.getCuisine());
        }

        return new ArrayList<>(cuisines);
    }

    public List<Restaurant> filter(Map<String, String> searchParams) {
        List<Restaurant> searchResults = new ArrayList<>(restaurants);

        for (Restaurant r : restaurants) {
            if (!r.getName().toLowerCase().contains(searchParams.get("searchTerm").toLowerCase())) {
                searchResults.remove(r);
            }

            if (!searchParams.get("selectedCity").equals("All")
                    && !r.getCity().equals(searchParams.get("selectedCity"))) {
                searchResults.remove(r);
            }

            if (!searchParams.get("selectedArea").equals("All")
                    && !r.getArea().equals(searchParams.get("selectedArea"))) {
                searchResults.remove(r);
            }

            if (!searchParams.get("selectedCuisine").equals("All")) {
                boolean isPresent = false;

                for (String cuisine : r.getCuisine()) {
                    if (cuisine.equals(searchParams.get("selectedCuisine"))) {
                        isPresent = true;
                        break;
                    }
                }

                if (!isPresent) {
                    searchResults.remove(r);
                }
            }

            if (!searchParams.get("selectedRating").equals("Any")) {
                double rating = Double.parseDouble(r.getRating());
                double selectedRating = Double.parseDouble(searchParams.get("selectedRating"));

                if (rating < selectedRating) {
                    searchResults.remove(r);
                }
            }

            if (searchParams.get("cashSelected").equals("true")) {
                boolean found = false;

                for (String paymentMethod : r.getPayment()) {
                    if (paymentMethod.equals("cash")) {
                        found = true;
                    }
                }

                if (!found) {
                    searchResults.remove(r);
                }
            }

            if (searchParams.get("cardSelected").equals("true")) {
                boolean found = false;

                for (String paymentMethod : r.getPayment()) {
                    if (paymentMethod.equals("card")) {
                        found = true;
                    }
                }

                if (!found) {
                    searchResults.remove(r);
                }
            }
        }

        return searchResults;
    }

    public List<Meal> getMeals() {
        List<Meal> meals = new ArrayList<>();
        for (Restaurant r : restaurants) {
            meals.addAll(r.getMeals());
        }

        return meals;
    }

    public Meal getMeal(String name) {
        for (Meal m : getMeals()) {
            if (m.getName().equals(name)) {
                return m;
            }
        }

        return null;
    }

    public Restaurant getRestaurant(String name) {
        for (Restaurant r : restaurants) {
            if (r.getName().equals(name)) {
                return r;
            }
        }

        return null;
    }
}
