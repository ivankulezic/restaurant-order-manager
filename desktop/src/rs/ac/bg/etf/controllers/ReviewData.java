package rs.ac.bg.etf.controllers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import rs.ac.bg.etf.models.Review;

public class ReviewData {
    private static final String DATA_LOCATION = "src/rs/ac/bg/etf/resources/data/reviews.json";

    private static ReviewData instance;

    private List<Review> reviews;

    private ReviewData() {

    }

    public static ReviewData getInstance() {

        if (instance == null) {
            instance = new ReviewData();
            instance.initializeData();
        }

        return instance;
    }

    private void initializeData() {
        Gson gson = new Gson();
        try (JsonReader reader = new JsonReader(new FileReader(DATA_LOCATION))) {
            reviews = gson.fromJson(reader, new TypeToken<List<Review>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public List<Review> getRestaurantReviews(String username) {
        List<Review> result = new ArrayList<>();

        for (Review r : reviews) {
            if (r.getUsername().equals(username) && r.getMeal().equals("")) {
                result.add(r);
            }
        }

        return result;
    }

    public List<Review> getMealReviews(String username) {
        List<Review> result = new ArrayList<>();

        for (Review r : reviews) {
            if (r.getUsername().equals(username) && !r.getMeal().equals("")) {
                result.add(r);
            }
        }

        return result;
    }

    public void addReview(Review r) {
        for (Review review : reviews) {
            if (review.getMeal().equals(r.getMeal()) && review.getRestaurant().equals(r.getRestaurant())
                    && review.getUsername().equals(r.getUsername())) {
                return;
            }
        }
        reviews.add(r);
    }

    public void removeReview(Review r) {
        reviews.remove(r);
    }

    public void saveData() {
        try (Writer writer = new FileWriter(DATA_LOCATION)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(reviews, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
