package rs.ac.bg.etf.controllers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import rs.ac.bg.etf.models.User;

public class UserData {

    private static final String DATA_LOCATION = "src/rs/ac/bg/etf/resources/data/users.json";

    private static UserData instance;

    private String username = "";

    private boolean loggedIn = false;

    private List<User> users;

    private UserData() {

    }

    public static UserData getInstance() {

        if (instance == null) {
            instance = new UserData();
            instance.initializeData();
        }

        return instance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    private void initializeData() {
        Gson gson = new Gson();
        try (JsonReader reader = new JsonReader(new FileReader(DATA_LOCATION))) {
            users = gson.fromJson(reader, new TypeToken<List<User>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public boolean checkCredentials(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkUsername(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                return false;
            }
        }

        return true;
    }

    public void addUser(User user) {
        users.add(user);
        saveData();
    }

    public void logout() {
        loggedIn = false;
        username = "";
        Cart.getInstance().clear();
    }

    public void saveData() {
        try (Writer writer = new FileWriter(DATA_LOCATION)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(users, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User getUser() {
        for (User u : users) {
            if (u.getUsername().equals(username)) {
                return u;
            }
        }

        return null;
    }
}
