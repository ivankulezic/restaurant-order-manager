package rs.ac.bg.etf.controllers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import rs.ac.bg.etf.models.Order;

public class OrderData {
    private static final String DATA_LOCATION = "src/rs/ac/bg/etf/resources/data/orders.json";

    private static OrderData instance;

    private List<Order> orders;

    private OrderData() {

    }

    public static OrderData getInstance() {

        if (instance == null) {
            instance = new OrderData();
            instance.initializeData();
        }

        return instance;
    }

    private void initializeData() {
        Gson gson = new Gson();
        try (JsonReader reader = new JsonReader(new FileReader(DATA_LOCATION))) {
            orders = gson.fromJson(reader, new TypeToken<List<Order>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public List<Order> getOrders(String username) {
        List<Order> results = new ArrayList<>();

        for (Order o : orders) {
            if (o.getUsername().equals(UserData.getInstance().getUsername())) {
                results.add(o);
            }
        }
        return results;
    }

    public void addOrder(Order o) {
        orders.add(o);
    }

    public void saveData() {
        try (Writer writer = new FileWriter(DATA_LOCATION)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(orders, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
