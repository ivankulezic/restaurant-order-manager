package rs.ac.bg.etf.models;

import java.util.List;

public class Order {
    private String status;

    private List<OrderEntry> entries;

    private String name;

    private String username;

    private String phone;

    private String street;

    private String city;

    private String note;

    private String total;

    private String payment;

    private String date;

    public Order() {
        super();
    }

    public Order(String status, List<OrderEntry> entries, String name, String username, String phone, String street,
            String city, String note, String total, String payment, String date) {
        super();
        this.status = status;
        this.entries = entries;
        this.name = name;
        this.username = username;
        this.phone = phone;
        this.street = street;
        this.city = city;
        this.note = note;
        this.total = total;
        this.payment = payment;
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<OrderEntry> entries) {
        this.entries = entries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
