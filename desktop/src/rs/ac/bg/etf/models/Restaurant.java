package rs.ac.bg.etf.models;

import java.util.List;
import java.util.Map;

public class Restaurant {

    private String name;

    private String image;

    private String description;

    private String rating;

    private Map<String, String> address;

    private String phone;

    private String email;

    private Map<String, String> hours;

    private List<String> payment;

    private String city;

    private String area;

    private List<String> cuisine;

    private List<Meal> meals;

    private List<RestaurantComment> comments;

    public Restaurant() {

    }

    public Restaurant(String name, String image, String description, String rating, Map<String, String> address,
            String phone, String email, Map<String, String> hours, List<String> payment, String city, String area,
            List<String> cuisine, List<Meal> meals, List<RestaurantComment> comments) {
        super();
        this.name = name;
        this.image = image;
        this.description = description;
        this.rating = rating;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.hours = hours;
        this.payment = payment;
        this.city = city;
        this.area = area;
        this.cuisine = cuisine;
        this.meals = meals;
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Map<String, String> getAddress() {
        return address;
    }

    public void setAddress(Map<String, String> address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> getHours() {
        return hours;
    }

    public void setHours(Map<String, String> hours) {
        this.hours = hours;
    }

    public List<String> getPayment() {
        return payment;
    }

    public void setPayment(List<String> payment) {
        this.payment = payment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<String> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<String> cuisine) {
        this.cuisine = cuisine;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public List<RestaurantComment> getComments() {
        return comments;
    }

    public void setComments(List<RestaurantComment> comments) {
        this.comments = comments;
    }

    public void addComment(RestaurantComment comment) {
        comments.add(comment);
    }
}
