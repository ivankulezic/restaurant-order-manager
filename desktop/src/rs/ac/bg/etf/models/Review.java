package rs.ac.bg.etf.models;

public class Review {
    private String username;

    private String meal;

    private String restaurant;

    public Review() {
        super();
    }

    public Review(String username, String meal, String restaurant) {
        super();
        this.username = username;
        this.meal = meal;
        this.restaurant = restaurant;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

}
