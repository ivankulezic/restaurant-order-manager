package rs.ac.bg.etf.models;

import java.text.DecimalFormat;
import java.util.List;

public class Meal {
    private String name;

    private String image;

    private String description;

    private String price;

    private String category;

    private List<String> cuisine;

    private List<String> ingredients;

    private List<RestaurantComment> comments;

    public Meal() {

    }

    public Meal(String name, String image, String description, String price, String category, List<String> cuisine,
            List<String> ingredients, List<RestaurantComment> comments) {
        super();
        this.name = name;
        this.image = image;
        this.description = description;
        this.price = price;
        this.category = category;
        this.cuisine = cuisine;
        this.ingredients = ingredients;
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getCuisine() {
        return cuisine;
    }

    public void setCuisine(List<String> cuisine) {
        this.cuisine = cuisine;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public List<RestaurantComment> getComments() {
        return comments;
    }

    public void setComments(List<RestaurantComment> comments) {
        this.comments = comments;
    }

    public String getRating() {
        double sum = 0;

        for (RestaurantComment c : comments) {
            sum += Double.parseDouble(c.getRating());
        }

        sum /= comments.size();

        return new DecimalFormat("#.00").format(sum);
    }
    
    public void addComment(RestaurantComment comment) {
        comments.add(comment);
    }
}
