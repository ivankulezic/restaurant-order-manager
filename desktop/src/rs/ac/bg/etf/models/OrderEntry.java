package rs.ac.bg.etf.models;

public class OrderEntry {
    private Meal meal;

    private Integer count;

    public OrderEntry() {
        super();
    }

    public OrderEntry(Meal meal, Integer count) {
        super();
        this.meal = meal;
        this.count = count;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
