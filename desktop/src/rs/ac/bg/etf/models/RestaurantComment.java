package rs.ac.bg.etf.models;

public class RestaurantComment {
    private String title;

    private String description;

    private String rating;

    private String user;

    public RestaurantComment() {
    }

    public RestaurantComment(String title, String description, String rating, String user) {
        super();
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
