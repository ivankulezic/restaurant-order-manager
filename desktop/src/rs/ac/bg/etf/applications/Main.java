package rs.ac.bg.etf.applications;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rs.ac.bg.etf.ui.BrowseRestaurants;

public class Main extends Application {

    public static Stage window;

    public static Stage restaurantWindow;

    public static Stage mealWindow;

    public static final String APP_TITLE = "Restaurant Order Manager";

    public static final int MIN_WIDTH = 800;

    public static final int MIN_HEIGHT = 600;

    @Override
    public void start(Stage window) {
        try {
            Main.window = window;
            window.setMinHeight(Main.MIN_HEIGHT);
            window.setMinWidth(Main.MIN_WIDTH);
            window.setTitle(Main.APP_TITLE + " - Browse Restaurants");
            restaurantWindow = new Stage();
            restaurantWindow.initModality(Modality.APPLICATION_MODAL);
            mealWindow = new Stage();
            mealWindow.initModality(Modality.APPLICATION_MODAL);
            Main.switchScene(Main.window, new BrowseRestaurants(window, Main.MIN_WIDTH, Main.MIN_HEIGHT).getScene());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void switchScene(Stage window, Scene scene) {
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    public static void openSceneInNewStage(Stage window, Scene scene) {
        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }
}
