
import UIKit

class  RestaurantSearchCell: UITableViewCell {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: UIButton!
}
