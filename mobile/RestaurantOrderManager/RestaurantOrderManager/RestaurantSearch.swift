import UIKit

class RestaurantSearch: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var selectedRating: String = "1"
    
    var rating_values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rating_values.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rating_values[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRating = rating_values[row]
    }
    
    var searchResults = [Restaurant]()
    
    @IBOutlet weak var resName: UITextField!
    @IBOutlet weak var ratingPicker: UIPickerView!
    @IBOutlet weak var payment: UISwitch!
    @IBOutlet weak var table: UITableView!
    
    @IBAction func search(_ sender: Any) {
        searchResults = RestaurantData.instance.search(name: "", rating: "", payment: "")
        self.table.reloadData()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RestaurantData.instance.selectedRestaurant = searchResults[indexPath.row]
        self.performSegue(withIdentifier: "fromSearchRestaurants", sender: self)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantSearchCell", for: indexPath) as! RestaurantSearchCell
        
        cell.logo.image = UIImage(named: searchResults[indexPath.row].image)
        cell.title.text = searchResults[indexPath.row].name
        cell.rating.setTitle(String(searchResults[indexPath.row].rating) + "/10", for: UIControlState.normal)
        return cell
    }
}
