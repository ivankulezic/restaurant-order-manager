import UIKit

class CustomerDetails: UIViewController {
    
    @IBOutlet weak var fName: UITextField!
    @IBOutlet weak var lName: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var address: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fName.text = DeliveryData.instance.selectedDelivery?.customer.firstName
        lName.text = DeliveryData.instance.selectedDelivery?.customer.lastName
        phone.text = DeliveryData.instance.selectedDelivery?.customer.phone
        address.text = DeliveryData.instance.selectedDelivery?.customer.street
    }
    
}
