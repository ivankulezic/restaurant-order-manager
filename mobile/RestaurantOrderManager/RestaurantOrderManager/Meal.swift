import Foundation

class Meal {
    var image: String
    var title: String
    var price: Int
    var comments = [Comment]()
    var rating: String
    var cuisine: String
    var category: String
    var ingredients: [String]
    
    init(image: String, title: String, price: Int, rating: String, cuisine: String, category: String, ingredients: [String]) {
        self.image = image
        self.title = title
        self.price = price
        self.rating = rating
        self.cuisine = cuisine
        self.category = category
        self.ingredients = ingredients
    }
    
    func addComment(comment: Comment) {
        comments.append(comment)
    }
}
