import Foundation

class Comment {
    var title: String
    var description: String
    var username: String
    var rating: String
    
    init(title: String, description: String, username: String, rating: String) {
        self.title = title
        self.description = description
        self.username = username
        self.rating = rating
    }
}
