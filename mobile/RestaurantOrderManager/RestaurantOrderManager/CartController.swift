import UIKit

class CartController: UIViewController {
    
    @IBOutlet weak var selectionToggle: UISegmentedControl!
    @IBOutlet weak var history: UIView!
    @IBOutlet weak var current: UIView!
    @IBAction func tabSwitched(_ sender: UISegmentedControl) {
        switch selectionToggle!.selectedSegmentIndex {
        case 0:
            history.isHidden = true
            current.isHidden = false
        case 1:
            history.isHidden = false
            current.isHidden = true
        default:
            break;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       history.isHidden = true
       current.isHidden = false
    }
    
}
