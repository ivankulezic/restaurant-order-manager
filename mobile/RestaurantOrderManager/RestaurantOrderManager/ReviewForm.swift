import UIKit

class ReviewForm: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var usertitle: UITextField!
    @IBOutlet weak var rating: UIPickerView!
    @IBOutlet weak var coment: UITextField!
    @IBOutlet weak var error: UILabel!
    
    @IBAction func submit(_ sender: Any) {
        
        usertitle.layer.borderColor = UIColor.red.cgColor
        coment.layer.borderColor = UIColor.red.cgColor
        
        usertitle.layer.borderWidth = 0.0
        coment.layer.borderWidth = 0.0
        self.error.text = ""
        
        var error: Bool = false
        
        if usertitle.text!.isEmpty {
            error = true
            usertitle.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if coment.text!.isEmpty {
            error = true
            coment.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if error {
            return
        }
        
        if Cart.instance.selectedReview!.meal == nil {
            Cart.instance.selectedReview!.restaurant!.addComment(comment: Comment(title: usertitle.text!, description: coment.text!, username: UserData.instance.username, rating: selectedRating))
        } else {
            Cart.instance.selectedReview!.meal!.addComment(comment: Comment(title: usertitle.text!, description: coment.text!, username: UserData.instance.username, rating: selectedRating))
        }
        
        ReviewData.instance.removeReview(review: Cart.instance.selectedReview!)
        
        _ = navigationController?.popViewController(animated: true)
        navigationController?.view.setNeedsDisplay()
        
        showAlert()
    }
    
    var selectedRating: String = "1"
    
    var rating_values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rating_values.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rating_values[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRating = rating_values[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let review = Cart.instance.selectedReview!
        
        var image = ""
        var title = ""
        
        if review.meal == nil {
            image = review.restaurant!.image
            title = review.restaurant!.name
        } else {
            image = review.meal!.image
            title = review.meal!.title
        }
        
        logo.image = UIImage(named: image)
        name.text = title
    }
    
    func showAlert() {
        let alertView = UIAlertController(title: "Review Submitted", message: "You have submitted your review successfully.", preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}
