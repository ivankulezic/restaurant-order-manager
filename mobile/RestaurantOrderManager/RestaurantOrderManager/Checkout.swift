import UIKit

class Checkout: UIViewController {
    
    @IBOutlet weak var fName: UILabel!
    @IBOutlet weak var lName: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var payment: UISwitch!
    @IBOutlet weak var total: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = UserData.instance.getUser()!
        
        fName.text = user.firstName
        lName.text = user.lastName
        phone.text = user.phone
        street.text = user.street
        total.text = Cart.instance.getTotal() + ",00 RSD"
    }
    
    @IBAction func confirmOrder(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        navigationController?.view.setNeedsDisplay()
        var paymentType = ""
        
        if payment.isOn {
            paymentType = "card"
        } else {
            paymentType = "cash"
        }
        
        let delivery = Delivery(restaurant: RestaurantData.instance.restaurants[0], time: "30", price: Cart.instance.getTotal(), customer: UserData.instance.getUser()!, status: "not delivered", distance: 1.5, payment: paymentType)
        delivery.addMeals(meals: Cart.instance.meals)
        DeliveryData.instance.addDelivery(delivery: delivery)
        Cart.instance.meals = [Meal]()
        showAlert(title: "Order Confirmed", message: "Your order is on it's way.", status: "OK")
        
    }
    
    func showAlert(title: String, message: String, status: String) {
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: status, style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}
