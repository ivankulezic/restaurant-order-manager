
import UIKit

class  MealSearchCell: UITableViewCell {
    
    @IBOutlet weak var imageMeal: UIImageView!
    @IBOutlet weak var mealName: UILabel!
    @IBOutlet weak var price: UILabel!
}
