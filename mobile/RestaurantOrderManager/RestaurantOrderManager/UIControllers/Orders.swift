import UIKit

class Orders: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DeliveryData.instance.getDeliveriesCount()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.table.reloadData()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! OrderCellTableViewCell
        cell.imageLogo.image = UIImage(named: DeliveryData.instance.deliveries[indexPath.row].restaurant.image)
        cell.streetText.text = DeliveryData.instance.deliveries[indexPath.row].customer.street
        cell.titleText.text = DeliveryData.instance.deliveries[indexPath.row].restaurant.name
        cell.timeText.setTitle(DeliveryData.instance.deliveries[indexPath.row].time + " min", for: UIControlState.normal)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DeliveryData.instance.selectDelivery(index: indexPath.row)
        self.performSegue(withIdentifier: "fromOrderList", sender: self)
    }
}
