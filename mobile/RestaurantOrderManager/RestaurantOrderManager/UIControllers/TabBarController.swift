import UIKit

class TabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Welcome " + UserData.instance.getUsername(), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doNothing))
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.plain, target: self, action: #selector(logout))
        self.navigationItem.title = "Delivery Home"
    }
    
    @objc
    func doNothing() {
        
    }
    
    @objc
    func logout(){
        UserData.instance.logout()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let welcome = storyBoard.instantiateViewController(withIdentifier: "welcomeScreen")
        self.navigationController?.pushViewController(welcome, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
