import UIKit

class Login: UIViewController {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        usernameField.layer.borderColor = UIColor.red.cgColor
        passwordField.layer.borderColor = UIColor.red.cgColor
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        let username = usernameField!.text
        let password = passwordField!.text
        
        errorMessage.text = ""
        usernameField.layer.borderWidth = 0.0
        passwordField.layer.borderWidth = 0.0
        
        var error: Bool = false
        
        if username!.isEmpty {
            error = true
            usernameField.layer.borderWidth = 1.0
            errorMessage.text = "Please fill all fields."
        }
        
        if password!.isEmpty {
            error = true
            passwordField.layer.borderWidth = 1.0
            errorMessage.text = "Please fill all fields."
        }
        
        if error {
            return
        }
        
        if UserData.instance.checkCredentials(username: username!, password: password!) {
            UserData.instance.setLoggedIn(username: username!)
            if UserData.instance.getUserType(username: username!) == "customer" {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
                let customerPage = storyBoard.instantiateViewController(withIdentifier: "CustomerTab")
                self.navigationController?.pushViewController(customerPage, animated: true)
            } else if UserData.instance.getUserType(username: username!) == "delivery" {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
                let deliveryPage = storyBoard.instantiateViewController(withIdentifier: "tabController")
                self.navigationController?.pushViewController(deliveryPage, animated: true)
            }
        } else {
            showAlert()
        }
    }
    
    func showAlert() -> Void {
        let alert = UIAlertController(title: "Login Alert", message: "Incorrect credentials. Please try again or register if you do not have an account.", preferredStyle: UIAlertControllerStyle.alert)
        
        let close = UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in self.dismiss(animated: true, completion: nil)})
        
        alert.addAction(close)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
