import UIKit

class DeliveryProfileView: UIViewController {
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var selectionToggle: UISegmentedControl!
    
    @IBAction func tabSwitched(_ sender: UISegmentedControl) {
        switch selectionToggle!.selectedSegmentIndex {
        case 0:
            changePasswordView.isHidden = true
            detailsView.isHidden = false
        case 1:
            changePasswordView.isHidden = false
            detailsView.isHidden = true
        default:
            break;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changePasswordView.isHidden = true
        detailsView.isHidden = false
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
