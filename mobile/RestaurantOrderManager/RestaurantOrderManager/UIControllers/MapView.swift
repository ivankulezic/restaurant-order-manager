import UIKit
import MapKit
import CoreLocation

class MapView: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var points = [Artwork]()
    
    let regionRadius: CLLocationDistance = 1000
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.showsUserLocation = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let initialLocation = CLLocation(latitude: 44.8051022, longitude: 20.4766086)
        centerMapOnLocation(location: initialLocation)

        for delivery in DeliveryData.instance.deliveries {
            let address = "Ulica " + delivery.customer.street + " " + delivery.customer.city
            CLGeocoder().geocodeAddressString(address) {
                placemarks, error in
                let placemark = placemarks?.first
                let artwork = Artwork(title: delivery.restaurant.name + " " + delivery.time + " min", coordinate: CLLocationCoordinate2D(latitude: placemark!.location!.coordinate.latitude, longitude: placemark!.location!.coordinate.longitude))
                self.mapView.addAnnotation(artwork)
            }
        }
    }
}
