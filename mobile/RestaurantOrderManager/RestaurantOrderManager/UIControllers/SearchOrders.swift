import UIKit

class SearchOrders: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var deliveryTime: UISlider!
    @IBOutlet weak var distance: UISlider!
    
    var searchResults = [Delivery]()
    
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var table: UITableView!
    
    @IBAction func filterDeliveries(_ sender: UIButton) {
        self.searchResults = DeliveryData.instance.filter(time: Int(deliveryTime.value), distance: Float(String(format: "%.01f", distance.value))!)
        self.table.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func deliveryTimeChanged(_ sender: UISlider) {
        deliveryTimeLabel.text = String(Int(sender.value)) + " min"
    }
    
    @IBAction func distanceChanged(_ sender: UISlider) {
        distanceLabel.text = String(format: "%.01f", sender.value) + " km"
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DeliveryData.instance.selectDelivery(index: indexPath.row)
        self.performSegue(withIdentifier: "fromSearchOrders", sender: self)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchCellTableView
        cell.logoImage.image = UIImage(named: searchResults[indexPath.row].restaurant.image)
        cell.address.text = searchResults[indexPath.row].customer.street
        cell.title.text = searchResults[indexPath.row].restaurant.name
        cell.time.setTitle(searchResults[indexPath.row].time + " min", for: UIControlState.normal)
        
        return cell
    }
}
