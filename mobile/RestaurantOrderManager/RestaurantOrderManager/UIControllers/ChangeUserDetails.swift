import UIKit

class ChangeUserDetails: UIView {
    
    @IBOutlet weak var firstnameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var streetText: UITextField!
    @IBOutlet weak var errorText: UILabel!
    
    @IBAction func saveChanges(_ sender: Any) {
        firstnameText.layer.borderColor = UIColor.red.cgColor
        lastNameText.layer.borderColor = UIColor.red.cgColor
        phoneText.layer.borderColor = UIColor.red.cgColor
        cityText.layer.borderColor = UIColor.red.cgColor
        streetText.layer.borderColor = UIColor.red.cgColor
        
        firstnameText.layer.borderWidth = 0.0
        lastNameText.layer.borderWidth = 0.0
        phoneText.layer.borderWidth = 0.0
        cityText.layer.borderWidth = 0.0
        streetText.layer.borderWidth = 0.0
        errorText.text = ""
        
        var error: Bool = false
        
        if firstnameText.text!.isEmpty {
            error = true
            firstnameText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if lastNameText.text!.isEmpty {
            error = true
            lastNameText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if phoneText.text!.isEmpty {
            error = true
            phoneText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if cityText.text!.isEmpty {
            error = true
            cityText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if streetText.text!.isEmpty {
            error = true
            streetText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if error {
            return
        }
        
        UserData.instance.updateUserData(firstname: firstnameText.text!, lastname: lastNameText.text!, phone: phoneText.text!, city: cityText.text!, street: streetText.text!)
        
        showAlert()
    }
    
    func showAlert() {
        let alertView = UIAlertController(title: "Update Successful", message: "You have updated your data successfully.", preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
}
