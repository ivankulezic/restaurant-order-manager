import UIKit

class ChangePasswordDelivery: UIView {
    
    
    @IBOutlet weak var oldPassText: UITextField!
    @IBOutlet weak var newPassText: UITextField!
    @IBOutlet weak var repeatText: UITextField!
    @IBOutlet weak var errorText: UILabel!
    
    @IBAction func changePassword(_ sender: Any) {
        
        oldPassText.layer.borderColor = UIColor.red.cgColor
        newPassText.layer.borderColor = UIColor.red.cgColor
        repeatText.layer.borderColor = UIColor.red.cgColor
        
        oldPassText.layer.borderWidth = 0.0
        newPassText.layer.borderWidth = 0.0
        repeatText.layer.borderWidth = 0.0
        errorText.text = ""
        
        var error: Bool = false
        
        if oldPassText.text!.isEmpty {
            error = true
            oldPassText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if newPassText.text!.isEmpty {
            error = true
            newPassText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if repeatText.text!.isEmpty {
            error = true
            repeatText.layer.borderWidth = 1.0
            errorText.text = "Please fill all fields."
        }
        
        if error {
            return
        }
        
        if !UserData.instance.checkCredentials(username: UserData.instance.getUsername(), password: oldPassText.text!) {
            oldPassText.layer.borderWidth = 1.0
            errorText.text = "Incorrect old password."
            return
        }
        
        if newPassText.text! != repeatText.text! {
            errorText.text = "Passwords do not match."
            repeatText.layer.borderWidth = 1.0
            newPassText.layer.borderWidth = 1.0
            return
        }
        
        UserData.instance.updateUserPassword(password: newPassText.text!)
        
        showAlert()
    }
    
    func showAlert() {
        let alertView = UIAlertController(title: "Update Successful", message: "You have changed your password successfully.", preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}
