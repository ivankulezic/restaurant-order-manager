import UIKit

class Register: UIViewController {
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorMessageText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        firstNameField.layer.borderColor = UIColor.red.cgColor
        lastNameField.layer.borderColor = UIColor.red.cgColor
        phoneField.layer.borderColor = UIColor.red.cgColor
        streetField.layer.borderColor = UIColor.red.cgColor
        cityField.layer.borderColor = UIColor.red.cgColor
        usernameField.layer.borderColor = UIColor.red.cgColor
        passwordField.layer.borderColor = UIColor.red.cgColor
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        let firstName = firstNameField!.text
        let lastName = lastNameField!.text
        let phone = phoneField!.text
        let city = cityField!.text
        let street = streetField!.text
        let username = usernameField!.text
        let password = passwordField!.text
        
        errorMessageText.text = ""
        firstNameField.layer.borderWidth = 0.0
        lastNameField.layer.borderWidth = 0.0
        phoneField.layer.borderWidth = 0.0
        cityField.layer.borderWidth = 0.0
        streetField.layer.borderWidth = 0.0
        usernameField.layer.borderWidth = 0.0
        passwordField.layer.borderWidth = 0.0

        var error: Bool = false
        
        if firstName!.isEmpty {
            error = true
            firstNameField.layer.borderWidth = 1.0
        }
        
        if lastName!.isEmpty {
            error = true
            lastNameField.layer.borderWidth = 1.0
        }
        
        if phone!.isEmpty {
            error = true
            phoneField.layer.borderWidth = 1.0
        }
        
        if city!.isEmpty {
            error = true
            cityField.layer.borderWidth = 1.0
        }
        
        if street!.isEmpty {
            error = true
            streetField.layer.borderWidth = 1.0
        }
        
        if username!.isEmpty {
            error = true
            usernameField.layer.borderWidth = 1.0
        }
        
        if password!.isEmpty {
            error = true
            passwordField.layer.borderWidth = 1.0
        }
        
        if error {
            errorMessageText.text = "Please fill all fields."
            return
        }
        
        if !UserData.instance.checkIfUsernameAvailable(username: username!) {
            errorMessageText.text = "Username is alredy taken."
            usernameField.layer.borderWidth = 1.0
            return
        } else {
            UserData.instance.addUser(user: User(username:username!, password:password!, firstName:firstName!, lastName:lastName!, phone:phone!, street:street!, city:city!, type: "customer"))
            showAlert(title: "Registration Successful", message: "You have successfully created an account. Redirecting you to login screen.", actionTitle: "OK", style: UIAlertActionStyle.default)
        }
        
    }
    
    func showAlert(title: String, message: String, actionTitle: String, style: UIAlertActionStyle) -> Void {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let action = UIAlertAction(title: actionTitle, style: style, handler: {(alert: UIAlertAction!) in         let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginPage = storyBoard.instantiateViewController(withIdentifier: "loginPage") as! Login
            self.navigationController?.pushViewController(loginPage, animated: true)})
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
