
import UIKit

class CurrentOrder: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBAction func checkoutPressed(_ sender: UIButton) {
        if Cart.instance.meals.count == 0 {
            showAlert(title: "Cart Empty", message: "Please add some items to the cart before checkout.", status: "Close")
        } else {
            let vc = UIStoryboard.init(name: "Customer", bundle: Bundle.main).instantiateViewController(withIdentifier: "checkout")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBOutlet weak var table: UITableView!
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.table.reloadData()
    }
    
    func showAlert(title: String, message: String, status: String) {
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: status, style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cart.instance.meals.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let meal = Cart.instance.meals[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! OrderCell

        cell.mealName.text = meal.title
        cell.price.text = String(meal.price) + ",00 RSD"
        cell.mealPhoto.image = UIImage(named: meal.image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            Cart.instance.meals.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Remove"
    }

}
