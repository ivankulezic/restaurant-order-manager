import UIKit

class  ReviewListCell: UITableViewCell {
    
    @IBOutlet weak var reviewImage: UIImageView!
    @IBOutlet weak var title: UILabel!
}
