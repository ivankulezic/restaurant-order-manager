import Foundation
import UIKit

class CustomerDetailsChange: UIViewController {
    
    @IBOutlet weak var fname: UITextField!
    @IBOutlet weak var lname: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var error: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = UserData.instance.getUser()!
        
        fname.text! = user.firstName
        lname.text! = user.lastName
        phone.text! = user.phone
        city.text! = user.city
        street.text = user.street
    }
    
    @IBAction func saveChanges(_ sender: Any) {
        fname.layer.borderColor = UIColor.red.cgColor
        lname.layer.borderColor = UIColor.red.cgColor
        phone.layer.borderColor = UIColor.red.cgColor
        city.layer.borderColor = UIColor.red.cgColor
        street.layer.borderColor = UIColor.red.cgColor
        
        fname.layer.borderWidth = 0.0
        lname.layer.borderWidth = 0.0
        phone.layer.borderWidth = 0.0
        city.layer.borderWidth = 0.0
        street.layer.borderWidth = 0.0
        self.error.text = ""
        
        var error: Bool = false
        
        if fname.text!.isEmpty {
            error = true
            fname.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if lname.text!.isEmpty {
            error = true
            lname.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if phone.text!.isEmpty {
            error = true
            phone.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if city.text!.isEmpty {
            error = true
            city.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if street.text!.isEmpty {
            error = true
            street.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if error {
            return
        }
        
        UserData.instance.updateUserData(firstname: fname.text!, lastname: lname.text!, phone: phone.text!, city: city.text!, street: street.text!)
        
        showAlert()
    }
    
    func showAlert() {
        let alertView = UIAlertController(title: "Update Successful", message: "You have updated your data successfully.", preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}
