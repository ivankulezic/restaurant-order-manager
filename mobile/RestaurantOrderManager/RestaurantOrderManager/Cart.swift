

import Foundation

class Cart {
    
    var selectedMeal: Meal? = nil
    
    var selectedOrder: Delivery? = nil
    
    var selectedReview: Review? = nil
    
    static let instance = Cart()
    
    var meals: [Meal]
    
    private init() {
        self.meals = [Meal]()
    }
    
    func addToCart(meal: Meal) {
        meals.append(meal)
    }
    
    func getTotal() -> String {
        var sum: Int = 0
        
        for meal in meals {
            sum += meal.price
        }
        
        return String(sum)
    }

}
