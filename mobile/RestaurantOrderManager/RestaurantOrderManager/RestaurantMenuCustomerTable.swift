import UIKit

class RestaurantMenuCustomerTable: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RestaurantData.instance.selectedRestaurant!.meals.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let meal = RestaurantData.instance.selectedRestaurant!.meals[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantMenuTableCell", for: indexPath) as! RestaurantMenuTableCell
        cell.imageLogo.image = UIImage(named: meal.image)
        cell.title.text = meal.title
        cell.price.text = String(meal.price) + ",00 RSD"
        cell.meal = meal
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Cart.instance.selectedMeal = RestaurantData.instance.selectedRestaurant!.meals[indexPath.row]
        
        let vc = UIStoryboard.init(name: "Customer", bundle: Bundle.main).instantiateViewController(withIdentifier: "mealDetails")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
