import UIKit

class BrowseRestaurantsCell: UITableViewCell {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var rating: UIButton!
}
