import UIKit

class OrderHistoryTable: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DeliveryData.instance.getDeliveries(username: UserData.instance.username).count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderHistoryCell", for: indexPath) as! OrderHistoryCell
        let delivery = DeliveryData.instance.getDeliveries(username: UserData.instance.username)[indexPath.row]
        cell.resImage.image = UIImage(named: delivery.restaurant.image)
        cell.resTitle.text = delivery.restaurant.name
        cell.price.text = delivery.price + ",00 RSD"
        cell.status.setTitle(delivery.status, for: UIControlState.normal)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       Cart.instance.selectedOrder = DeliveryData.instance.getDeliveries(username: UserData.instance.username)[indexPath.row]
        
        let vc = UIStoryboard.init(name: "Customer", bundle: Bundle.main).instantiateViewController(withIdentifier: "oldOrderDetails")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
