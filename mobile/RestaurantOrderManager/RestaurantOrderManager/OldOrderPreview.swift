import UIKit

class OldOrderPreview: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var fname: UILabel!
    @IBOutlet weak var lname: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var payment: UILabel!
    @IBOutlet weak var total: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let delivery = Cart.instance.selectedOrder!
        
        fname.text = delivery.customer.firstName
        lname.text = delivery.customer.lastName
        phone.text = delivery.customer.phone
        street.text = delivery.customer.street
        payment.text = delivery.payment
        total.text = delivery.price + ",00 RSD"
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cart.instance.selectedOrder!.meals.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "oldOrderCell", for: indexPath) as! OldOrderCell
        let meal = Cart.instance.selectedOrder!.meals[indexPath.row]
        cell.mealImage.image = UIImage(named: meal.image)
        cell.mealPrice.setTitle(String(meal.price) + ",00 RSD", for: UIControlState.normal)
        cell.mealTitle.text = meal.title
        
        return cell
    }
}
