import UIKit

class ProfileController: UIViewController {
    
    @IBOutlet weak var toggle: UISegmentedControl!
    @IBOutlet weak var userData: UIView!
    @IBOutlet weak var changePassword: UIView!
    @IBOutlet weak var reviews: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userData.isHidden = false
        changePassword.isHidden = true
        reviews.isHidden = true
    }
    
    @IBAction func tabChanged(_ sender: UISegmentedControl) {
        switch toggle!.selectedSegmentIndex {
        case 0:
            userData.isHidden = false
            changePassword.isHidden = true
            reviews.isHidden = true
        case 1:
            userData.isHidden = true
            changePassword.isHidden = false
            reviews.isHidden = true
        case 2:
            userData.isHidden = true
            changePassword.isHidden = true
            reviews.isHidden = false
        default:
            break;
        }
    }
    
}
