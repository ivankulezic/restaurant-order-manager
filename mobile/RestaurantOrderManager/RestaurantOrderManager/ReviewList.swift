import UIKit

class ReviewList: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.table.reloadData()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ReviewData.instance.reviews.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let review = ReviewData.instance.reviews[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewListCell", for: indexPath) as! ReviewListCell
        
        var image = ""
        var title = ""
        if review.meal == nil {
            image = review.restaurant!.image
            title = review.restaurant!.name
        } else {
            image = review.meal!.image
            title = review.meal!.title
        }
        
        cell.reviewImage.image = UIImage(named: image)
        cell.title.text = title
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Cart.instance.selectedReview = ReviewData.instance.reviews[indexPath.row]
        
        let vc = UIStoryboard.init(name: "Customer", bundle: Bundle.main).instantiateViewController(withIdentifier: "reviewForm")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
