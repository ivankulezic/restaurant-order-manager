import UIKit

class OrderCell: UITableViewCell {
    
    @IBOutlet weak var mealName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var mealPhoto: UIImageView!
}
