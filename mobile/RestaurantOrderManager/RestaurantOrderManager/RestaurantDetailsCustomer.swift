import UIKit
import MapKit

class RestaurantDetailsCustomer: UIViewController {
    
    
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 300
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let restaurant = RestaurantData.instance.selectedRestaurant

        imageLogo.image = UIImage(named: restaurant!.image)
        address.text = restaurant!.street
        phone.text = restaurant!.phone
        rating.text = String(restaurant!.rating) + " / 10"
        email.text = restaurant!.email
        
        let initialLocation = CLLocation(latitude: restaurant!.latitude, longitude: restaurant!.longitude)
        centerMapOnLocation(location: initialLocation)
        let artwork = Artwork(title: restaurant!.name,
                              coordinate: CLLocationCoordinate2D(latitude: restaurant!.latitude, longitude: restaurant!.longitude))
        mapView.addAnnotation(artwork)
    }
    
}
