import Foundation
import UIKit

class CustomerPasswordChange: UIViewController {
    
    @IBOutlet weak var old: UITextField!
    @IBOutlet weak var new: UITextField!
    @IBOutlet weak var again: UITextField!
    @IBOutlet weak var error: UILabel!
    
    @IBAction func changePass(_ sender: Any) {
        old.layer.borderColor = UIColor.red.cgColor
        new.layer.borderColor = UIColor.red.cgColor
        again.layer.borderColor = UIColor.red.cgColor
        
        old.layer.borderWidth = 0.0
        new.layer.borderWidth = 0.0
        again.layer.borderWidth = 0.0
        self.error.text = ""
        
        var error: Bool = false
        
        if old.text!.isEmpty {
            error = true
            old.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if new.text!.isEmpty {
            error = true
            new.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if again.text!.isEmpty {
            error = true
            again.layer.borderWidth = 1.0
            self.error.text = "Please fill all fields."
        }
        
        if error {
            return
        }
        
        if !UserData.instance.checkCredentials(username: UserData.instance.getUsername(), password: old.text!) {
            old.layer.borderWidth = 1.0
            self.error.text = "Incorrect old password."
            return
        }
        
        if new.text! != again.text! {
            self.error.text = "Passwords do not match."
            again.layer.borderWidth = 1.0
            new.layer.borderWidth = 1.0
            return
        }
        
        UserData.instance.updateUserPassword(password: new.text!)
        
        showAlert()
    }
    
    func showAlert() {
        let alertView = UIAlertController(title: "Update Successful", message: "You have changed your password successfully.", preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}
