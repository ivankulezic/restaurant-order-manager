

import Foundation

class DeliveryData {
    static let instance = DeliveryData()
    
    var deliveries:[Delivery]
    
    var done = [Delivery]()
    
    var uniqueID: Int = 0
    
    var selectedDelivery: Delivery? = nil
    
    private init() {
        self.deliveries = [Delivery]()
        
        var delivery = Delivery(restaurant: RestaurantData.instance.restaurants[0], time: "60", price: "520", customer: UserData.instance.users[2], status: "not delivered", distance: 2.5, payment: "cash")
        delivery.addMeals(meals: [RestaurantData.instance.restaurants[0].meals[0], RestaurantData.instance.restaurants[0].meals[1]])
        addDelivery(delivery: delivery)
        
        delivery = Delivery(restaurant: RestaurantData.instance.restaurants[1], time: "15", price: "180", customer: UserData.instance.users[3], status: "not delivered", distance: 1.7, payment: "card")
        addDelivery(delivery: delivery)
        delivery.addMeals(meals: [RestaurantData.instance.restaurants[1].meals[1]])
        
        delivery = Delivery(restaurant: RestaurantData.instance.restaurants[1], time: "45", price: "450", customer: UserData.instance.users[4], status: "not delivered", distance: 1.5, payment: "cash")
        addDelivery(delivery: delivery)
        delivery.addMeals(meals: [RestaurantData.instance.restaurants[1].meals[0]])
        
        delivery = Delivery(restaurant: RestaurantData.instance.restaurants[0], time: "25", price: "820", customer: UserData.instance.users[5], status: "not delivered", distance: 2.4, payment: "card")
        addDelivery(delivery: delivery)
        delivery.addMeals(meals: [RestaurantData.instance.restaurants[0].meals[0], RestaurantData.instance.restaurants[0].meals[1], RestaurantData.instance.restaurants[0].meals[1]])
        
        delivery = Delivery(restaurant: RestaurantData.instance.restaurants[0], time: "75", price: "300", customer: UserData.instance.users[6], status: "not delivered", distance: 3.5, payment: "cash")
        addDelivery(delivery: delivery)
        delivery.addMeals(meals: [RestaurantData.instance.restaurants[0].meals[1]])
    }
    
    func selectDelivery(index: Int) -> Void {
        self.selectedDelivery = deliveries[index]
    }
    
    func filter(time: Int, distance: Float) -> [Delivery] {
        var results = [Delivery]()
        
        for delivery in deliveries {
            if Int(delivery.time)! <= time && delivery.distance <= distance {
                results.append(delivery)
            }
        }
        
        return results
    }
    
    func removeDelivery(id: Int, status: Bool) {
        for index in 0..<deliveries.count {
            if deliveries[index].id == id {
                let d = deliveries.remove(at: index)
                if status {
                    d.status = "successful"
                } else {
                    d.status = "canceled"
                }
                self.done.append(d)
                return
            }
        }
    }
    
    func addDelivery(delivery: Delivery) {
        delivery.id = uniqueID
        uniqueID += 1
        self.deliveries.append(delivery)
    }
    
    func getDeliveriesCount() -> Int {
        return self.deliveries.count
    }
    
    func getDeliveries(username: String) -> [Delivery] {
        var result = [Delivery]()
        
        for delivery in deliveries {
            if delivery.customer.username == username {
                result.append(delivery)
            }
        }
        
        for delivery in done {
            if delivery.customer.username == username {
                result.append(delivery)
            }
        }
        
        return result
    }
}
