import UIKit

class MealsSearch: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pickerView.selectRow(1, inComponent: 0, animated: true)
        pickerView.selectRow(1, inComponent: 1, animated: true)
    }
    
    @IBOutlet weak var table: UITableView!
    var category_values = ["", "breakfast", "lunch", "dinner"]
    var cuisine_values = ["", "serbian", "mexican", "barbecue", "traditional"]
    
    @IBAction func search(_ sender: Any) {
        searchResults = RestaurantData.instance.getMeals()
        table.reloadData()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return cuisine_values.count
        }
        
        return category_values.count
    }
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return cuisine_values[row]
        }
        return category_values[row]
    }
    
    var searchResults = [Meal]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Cart.instance.selectedMeal = searchResults[indexPath.row]
        self.performSegue(withIdentifier: "fromMealSearch", sender: self)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealSearchCell", for: indexPath) as! MealSearchCell
        let meal = searchResults[indexPath.row]
        cell.imageMeal.image = UIImage(named: meal.image)
        cell.mealName.text = meal.title
        cell.price.text = String(meal.price) + ",00 RSD"
        return cell
    }
}

