import UIKit

class MealCommentCell: UITableViewCell {
    
    @IBOutlet weak var initials: UIButton!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var rating: UIButton!
}
