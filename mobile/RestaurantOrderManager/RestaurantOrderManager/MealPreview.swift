import UIKit

class MealPreview: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var mealTitle: UILabel!
    @IBOutlet weak var mealrating: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var cuisine: UILabel!
    @IBOutlet weak var ingredients: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let meal = Cart.instance.selectedMeal!
        
        mealImage.image = UIImage(named: meal.image)
        mealTitle.text = meal.title
        mealrating.text = meal.rating + " /10"
        category.text = meal.category
        cuisine.text = meal.cuisine
        ingredients.text = meal.ingredients.joined(separator: ",")
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cart.instance.selectedMeal!.comments.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let comment = Cart.instance.selectedMeal!.comments[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCommentCell", for: indexPath) as! MealCommentCell
        cell.title.text = comment.title
        cell.initials.setTitle(String(Array(UserData.instance.getUser(username: comment.username)!.firstName)[0]) + String(Array(UserData.instance.getUser(username: comment.username)!.lastName)[0]), for: UIControlState.normal)
        cell.comment.text = comment.description
        cell.rating.setTitle(comment.rating + "/10", for: UIControlState.normal)
        
        return cell
    }
}
