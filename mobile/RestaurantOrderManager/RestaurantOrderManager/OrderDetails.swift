import UIKit

class OrderDetails: UIViewController {
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var time: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var customer: UIButton!
    @IBOutlet weak var restaurant: UIButton!
    @IBOutlet weak var status: UISwitch!
    
    @IBAction func finishDelivery(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
        navigationController?.view.setNeedsDisplay()
        
        DeliveryData.instance.removeDelivery(id: DeliveryData.instance.selectedDelivery!.id, status: status.isOn)
        showAlert(status: status.isOn)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let delivery = DeliveryData.instance.selectedDelivery
        
        address.text = delivery?.customer.street
        time.setTitle(delivery!.time + " min", for: UIControlState.normal)
        price.text = delivery!.price + ",00 RSD"
        customer.setTitle(delivery!.customer.firstName + " " + delivery!.customer.lastName, for: UIControlState.normal)
        restaurant.setTitle(delivery!.restaurant.name, for: UIControlState.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showAlert(status: Bool) {
        var deliveryStatus: String
        var message: String
        if status {
            deliveryStatus = "Delivery Successfull"
            message = "You have successfully delivered the order."
        } else {
            deliveryStatus = "Delivery Canceled"
            message = "You have cancelled the order."
        }
        
        let alertView = UIAlertController(title: deliveryStatus, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
}
