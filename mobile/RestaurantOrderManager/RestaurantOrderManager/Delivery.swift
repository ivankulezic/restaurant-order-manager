

import Foundation

class Delivery {
    var id: Int
    var restaurant: Restaurant
    var time: String
    var price: String
    var customer: User
    var status: String
    var distance: Float
    var meals = [Meal]()
    var payment: String
    
    init(restaurant: Restaurant, time: String, price: String, customer: User, status: String, distance: Float, payment: String) {
        self.id = 0
        self.restaurant = restaurant
        self.time = time
        self.price = price
        self.customer = customer
        self.status = status
        self.distance = distance
        self.payment = payment
    }
    
    func addMeal(meal: Meal) {
        meals.append(meal)
    }
    
    func addMeals(meals: [Meal]) {
        self.meals = meals
    }
}
