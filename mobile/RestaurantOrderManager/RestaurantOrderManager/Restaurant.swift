
import Foundation

class Restaurant {
    var image: String
    var name: String
    var street: String
    var city: String
    var phone: String
    var rating: Float
    var latitude: Double
    var longitude: Double
    var email: String
    var comments = [Comment]()
    var images = [String]()
    var meals = [Meal]()
    
    init(image: String, name: String, street: String, city: String, phone: String, rating: Float, latitude: Double, longitude: Double, email: String) {
        self.image = image
        self.name = name
        self.street = street
        self.city = city
        self.phone = phone
        self.rating = rating
        self.latitude = latitude
        self.longitude = longitude
        self.email = email
    }
    
    func addComment(comment: Comment) {
        comments.append(comment)
    }
    
    func addImage(image: String) {
        images.append(image)
    }
    
    func addMeal(meal: Meal) {
    meals.append(meal)
    }
}
