import UIKit

class SearchController: UIViewController {
    
    // restaurants and meals have switched names
    
    @IBOutlet weak var toggle: UISegmentedControl!
    @IBOutlet weak var restaurants: UIView!
    @IBOutlet weak var meals: UIView!
    
    @IBAction func toggleSwitched(_ sender: Any) {
        switch toggle!.selectedSegmentIndex {
        case 0:
            restaurants.isHidden = true
            meals.isHidden = false
        case 1:
            restaurants.isHidden = false
            meals.isHidden = true
        default:
            break;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restaurants.isHidden = true
        meals.isHidden = false
    }
}
