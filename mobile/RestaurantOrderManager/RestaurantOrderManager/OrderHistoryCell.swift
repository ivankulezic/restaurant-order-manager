
import UIKit

class  OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var resImage: UIImageView!
    @IBOutlet weak var resTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UIButton!
}
