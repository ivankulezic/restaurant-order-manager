import UIKit

class  RestaurantMenuTableCell: UITableViewCell {
    
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var meal: Meal? = nil
    
    @IBAction func addToCart(_ sender: UIButton) {
        if UserData.instance.isLoggedIn() {
            Cart.instance.addToCart(meal: meal!)
            print(Cart.instance.meals.count)
            showAlert(title: "Successfully Added Item", message: "You have added one " + meal!.title + " to cart.", status: "OK")
        } else {
            showAlert(title: "LogIn to Continue", message: "Please Log in before adding meals to cart.", status: "Close")
        }
    }
    
    func showAlert(title: String, message: String, status: String) {
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertView.addAction(UIAlertAction(title: status, style: UIAlertActionStyle.default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
    }
}
