import Foundation

class UserData {
    
    static let instance = UserData()
    
    var users:[User]
    var loggedIn: Bool
    var username: String
    
    private init() {
        self.users = [User]()
        self.loggedIn = false
        self.username = ""
        addUser(user: User(username:"ivan", password:"ivan123", firstName:"Ivan", lastName:"Kulezic", phone:"+38164123456", street:"Mije Kovacevica 9", city:"Belgrade", type: "delivery"))
        addUser(user: User(username:"a", password:"a", firstName:"Nikola", lastName:"Kulezic", phone:"+38164123456", street:"Mije Kovacevica 9", city:"Belgrade", type: "customer"))
        addUser(user: User(username:"petar", password:"pera", firstName:"Petar", lastName:"Trifunovic", phone:"+38164123456", street:"Dalmatinska 55", city:"Belgrade", type: "customer"))
        addUser(user: User(username:"ogi", password:"ogi123", firstName:"Ognjen", lastName:"Andric", phone:"+38164123456", street:"Zahumska 53", city:"Belgrade", type: "customer"))
        addUser(user: User(username:"zoki", password:"zokibab", firstName:"Zoran", lastName:"Babovic", phone:"+38164123456", street:"Resavska 33", city:"Belgrade", type: "customer"))
        addUser(user: User(username:"neca", password:"necika", firstName:"Nemanja", lastName:"Djuric", phone:"+38164123456", street:"Ruzveltova 25", city:"Belgrade", type: "customer"))
        addUser(user: User(username:"sale", password:"saki", firstName:"Sasa", lastName:"Maric", phone:"+38164123456", street:"Vladetina 12", city:"Belgrade", type: "customer"))
    }
    
    func addUser(user:User) -> Void {
        users.append(user)
    }
    
    func checkIfUsernameAvailable(username: String) -> Bool {
        for user in users{
            if user.username == username {
                return false
            }
        }
        
        return true
    }
    
    func getUserType(username: String) -> String {
        for user in users {
            if(user.username == username){
                return user.type
            }
        }
        
        return ""
    }
    
    func getUsername() -> String {
        return username
    }
    
    func checkCredentials(username: String, password: String) -> Bool {
        for user in users{
            if user.username == username && user.password == password {
                return true
            }
        }
        
        return false
    }
    
    func updateUserPassword(password: String) {
        for user in users {
            if username == user.username {
                user.password = password
            }
        }
    }
    
    func getUser() -> User? {
        for user in users {
            if user.username == username {
                return user
            }
        }
        
        return nil
    }
    
    func getUser(username: String) -> User? {
        for user in users {
            if user.username == username {
                return user
            }
        }
        
        return nil
    }
    
    func updateUserData(firstname: String, lastname: String, phone: String, city: String, street: String) -> Void {
        for user in users {
            if user.username == username {
                user.firstName = firstname
                user.lastName = lastname
                user.phone = phone
                user.city = city
                user.street = street
                return
            }
        }
    }
    
    func setLoggedIn(username: String) -> Void {
        self.username = username
        self.loggedIn = true
    }
    
    func logout() -> Void {
        self.username = ""
        self.loggedIn = false
    }
    
    func isLoggedIn() -> Bool {
        return self.loggedIn
    }
}
