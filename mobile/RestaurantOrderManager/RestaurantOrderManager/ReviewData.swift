import Foundation

class ReviewData {
    
    static let instance = ReviewData()
    
    var reviews: [Review]
    
    private init() {
        self.reviews = [Review]()
        
        addReview(review: Review(restaurant: RestaurantData.instance.restaurants[0], meal: nil, username: "ogi"))
        addReview(review: Review(restaurant: nil, meal: RestaurantData.instance.restaurants[0].meals[0], username: "ogi"))
        addReview(review: Review(restaurant: nil, meal: RestaurantData.instance.restaurants[0].meals[1], username: "ogi"))
    }
    
    func addReview(review: Review) {
        reviews.append(review)
    }
    
    func getReviews(username: String) -> [Review]{
        var result = [Review]()
        
        for review in reviews {
            if review.username == username {
                result.append(review)
            }
        }
        
        return result
    }
    
    func removeReview(review: Review) {
        var i = 0
        
        for (index, r) in reviews.enumerated() {
            if r.id == review.id{
                i = index
                break
            }
        }
        
        reviews.remove(at: i)
    }
}
