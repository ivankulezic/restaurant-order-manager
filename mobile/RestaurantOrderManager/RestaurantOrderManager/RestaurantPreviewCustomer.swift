import UIKit

class RestaurantPreviewCustomer: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RestaurantData.instance.restaurants.count
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RestaurantData.instance.selectedRestaurant = RestaurantData.instance.restaurants[indexPath.row]
        self.performSegue(withIdentifier: "toRestaurantDetails", sender: self)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let restaurant = RestaurantData.instance.restaurants[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantListCellCustomer", for: indexPath) as! CustomerRestaurantListCell
        cell.logoImage.image = UIImage(named: restaurant.image)
        cell.street.text = restaurant.street
        cell.title.text = restaurant.name
        cell.rating.setTitle(String(restaurant.rating) + " / 10", for: UIControlState.normal)
        
        return cell
    }
}
