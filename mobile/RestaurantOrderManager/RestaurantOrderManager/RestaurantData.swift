

import Foundation

class RestaurantData {
    static let instance = RestaurantData()
    
    var selectedRestaurant: Restaurant? = nil
    
    var restaurants:[Restaurant]
    
    private init() {
        self.restaurants = [Restaurant]()
        var restaurant = Restaurant(image: "walter.png", name: "Walter", street: "Dvadeset sedmog marta 31", city: "Belgrade", phone: "011234567", rating: 9.2, latitude: 44.810725, longitude: 20.4745853, email: "walter@gmail.com")
        restaurant.addComment(comment: Comment(title: "Great meal", description: "Cheap big meal.", username: "petar", rating: "9"))
        restaurant.addComment(comment: Comment(title: "So good", description: "I'm in Sarajevo.", username: "ogi", rating: "8"))
        restaurant.addComment(comment: Comment(title: "Mmmmm", description: "Best barbecue.", username: "zoki", rating: "7"))
        restaurant.addImage(image: "walter1.png")
        restaurant.addImage(image: "walter2.png")
        restaurant.addImage(image: "walter3.png")
        restaurant.addImage(image: "walter4.png")
        restaurant.addImage(image: "walter5.png")
        
        var meal = Meal(image: "walter_kabobs_5.jpg", title: "5 Sarajevo kabobs", price: 220, rating: "9.5", cuisine: "grill", category: "lunch", ingredients: ["beef", "onions", "somun"])
        meal.addComment(comment: Comment(title: "Good grill", description: "Big portion", username: "petar", rating: "9"))
        meal.addComment(comment: Comment(title: "Traditional", description: "Best Serbian meal", username: "ogi", rating: "7"))
        restaurant.addMeal(meal: meal)
        
        meal = Meal(image: "walter_chicken_breasts.jpg", title: "Chicken breasts", price: 300, rating: "8.2", cuisine: "serbian", category: "dinner", ingredients: ["chicken", "fries", "bread"])
        meal.addComment(comment: Comment(title: "Fine chicken", description: "Ask for well done.", username: "neca", rating: "10"))
        meal.addComment(comment: Comment(title: "Salty", description: "Too much salt", username: "zoki", rating: "6"))
        restaurant.addMeal(meal: meal)
        addRestaurant(restaurant: restaurant)
        
        restaurant = Restaurant(image: "burito_madre.png", name: "Burito Madre", street: "Terazije 27", city: "Belgrade", phone: "011234567", rating: 7.9, latitude: 44.812714, longitude: 20.4613576, email: "burito@madre.com")
        restaurant.addComment(comment: Comment(title: "Better than nothing", description: "really fast service.", username: "petar", rating: "7"))
        restaurant.addComment(comment: Comment(title: "Great meal", description: "Very little meat.", username: "ogi", rating: "6"))
        restaurant.addComment(comment: Comment(title: "Great meal", description: "Great if you are vegan.", username: "zoki", rating: "9"))
        restaurant.addImage(image: "burito_madre1.png")
        restaurant.addImage(image: "burito_madre2.png")
        restaurant.addImage(image: "burito_madre3.png")
        restaurant.addImage(image: "burito_madre4.png")
        
        meal = Meal(image: "burito_madre_burito.jpg", title: "Burito in tortilla", price: 450, rating: "8.1", cuisine: "mexican", category: "breakfast", ingredients: ["chicken", "vegetbles"])
        meal.addComment(comment: Comment(title: "Nice vegan meal", description: "Lots of salads.", username: "petar", rating: "9"))
        meal.addComment(comment: Comment(title: "Solid food", description: "Not much meat", username: "ogi", rating: "7"))
        restaurant.addMeal(meal: meal)
        
        meal = Meal(image: "burito_madre_lemonade.jpg", title: "Lemonade", price: 180, rating: "6.8", cuisine: "refreshment", category: "breakfast", ingredients: ["lemon", "water"])
        meal.addComment(comment: Comment(title: "Cool", description: "Nice cool drink.", username: "neca", rating: "10"))
        meal.addComment(comment: Comment(title: "Refreshing", description: "Too much water", username: "zoki", rating: "6"))
        
        restaurant.addMeal(meal: meal)
        addRestaurant(restaurant: restaurant)
    }
    
    func addRestaurant(restaurant: Restaurant) {
        restaurants.append(restaurant)
    }
    
    func search(name: String, rating: String, payment: String) -> [Restaurant] {
        return restaurants
    }
    
    func getMeals() -> [Meal] {
        var results = [Meal]()
        
        for r in restaurants {
            results.append(contentsOf: r.meals)
        }
        
        return results
    }
}
