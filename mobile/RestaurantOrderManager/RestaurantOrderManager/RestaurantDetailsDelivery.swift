import UIKit
import MapKit

class RestaurantDetailsDelivery: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 300
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = DeliveryData.instance.selectedDelivery!.restaurant.name
        imageLogo.image = UIImage(named: DeliveryData.instance.selectedDelivery!.restaurant.image)
        address.text = DeliveryData.instance.selectedDelivery!.restaurant.street
        phone.text = DeliveryData.instance.selectedDelivery!.restaurant.phone
        rating.text = String(DeliveryData.instance.selectedDelivery!.restaurant.rating) + " / 10"
        
        let initialLocation = CLLocation(latitude: DeliveryData.instance.selectedDelivery!.restaurant.latitude, longitude: DeliveryData.instance.selectedDelivery!.restaurant.longitude)
        centerMapOnLocation(location: initialLocation)
        let artwork = Artwork(title: DeliveryData.instance.selectedDelivery!.restaurant.name,
                              coordinate: CLLocationCoordinate2D(latitude: DeliveryData.instance.selectedDelivery!.restaurant.latitude, longitude: DeliveryData.instance.selectedDelivery!.restaurant.longitude))
        mapView.addAnnotation(artwork)
    }
    
}
