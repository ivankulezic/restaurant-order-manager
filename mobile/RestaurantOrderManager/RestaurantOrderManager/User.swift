import Foundation

class User {
    var username: String
    var password: String
    var firstName: String
    var lastName: String
    var phone: String
    var street: String
    var city: String
    var type: String
    
    init(username: String, password: String, firstName: String, lastName: String, phone: String, street: String,city: String, type: String) {
        self.username = username
        self.password = password
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.street = street
        self.city = city
        self.type = type
    }
}
