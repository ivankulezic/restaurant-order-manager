import UIKit

class RestaurantCommentsTable: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RestaurantData.instance.selectedRestaurant!.comments.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let comment = RestaurantData.instance.selectedRestaurant!.comments[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantCommentCell", for: indexPath) as! RestaurantCommentsCell
        cell.comment.text = comment.description
        cell.title.text = comment.title
 cell.initials.setTitle(String(Array(UserData.instance.getUser(username: comment.username)!.firstName)[0]) + String(Array(UserData.instance.getUser(username: comment.username)!.lastName)[0]), for: UIControlState.normal)
        cell.rating.setTitle(comment.rating + "/10", for: UIControlState.normal)
        
        return cell
    }
}
