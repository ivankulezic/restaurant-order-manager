import Foundation

class Review {
    
    var restaurant: Restaurant? = nil
    var meal: Meal? = nil
    var username: String
    var id: Int
    static var autoid = 0
    
    init(restaurant: Restaurant?, meal: Meal?, username: String) {
        self.meal = meal
        self.restaurant = restaurant
        self.username = username
        self.id = Review.autoid
        Review.autoid += 1
    }
}
